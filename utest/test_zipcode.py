#from django.test import TestCase
from unittest import TestCase
from somos.biz.slot_utils import SlotUtils
from os import environ


class ZipValidationTestCase(TestCase):
    def setUp(self):
        for item in environ.items():
            if 0:
                print(item)

    def test_res_code_is_2(self):
        res_code = SlotUtils.is_valid_zipcode(1397, "11106")
        self.assertEqual(res_code, 2)

    def test_res_code_is_0(self):
        res_code = SlotUtils.is_valid_zipcode(1397, "10451")
        res_code = SlotUtils.is_valid_zipcode(1365, "11106")
        self.assertEqual(res_code, 0)

    def test_get_restricted_olids(self):
        self.assertTrue(SlotUtils.get_zip_enforced_olids())
        self.assertTrue(1367 in SlotUtils.get_zip_enforced_olids())

    def test_get_zips_restricted(self):
        zip_dict = SlotUtils.get_olid_zip_dict()
        for i in map(lambda e: str(e), (10457, 10462, 10463, 10464, 10465, 10466)):
            self.assertTrue(i in zip_dict[1397])