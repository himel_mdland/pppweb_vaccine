FROM python:3.7-slim
RUN apt-get update && apt-get install gcc -y && apt-get clean
WORKDIR /code
COPY . .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 9000
#CMD ["python", "manage.py", "runsslserver","127.0.0.1:9000"]