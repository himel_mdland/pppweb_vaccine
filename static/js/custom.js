
var MG = {};
MG.Modal = {};

MG.Modal.initLoading = function () {
    if($('#mg_modal_loading').length){
        return;
    }
    $("body").append("<!-- loading -->" +
                "<div class='modal' id='mg_modal_loading' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' data-backdrop='static'>" +
                "<div class='modal-dialog' role='document'>" +
                "<div class='modal-content'>" +
                "<div class='modal-header'>" +
                "<h4 class='modal-title' id='mg_modal_loading_text_label'>Please wait</h4>" +
                "</div>" +
                "<div id='mg_modal_loading_text' class='modal-body'>" +
                "<i class='fa fa-spin fa-refresh'></i>" +
                " Loading..." +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>");
    }
MG.Modal.showLoading = function (text) {
    MG.Modal.initLoading()
    if (typeof (text) != 'undefined'){
        $("#mg_modal_loading_text").html("<i class='fa fa-spin fa-refresh'></i>&nbsp;" + text);
    }
    $("#mg_modal_loading").modal("show");
}

MG.Modal.hideLoading = function () {
    $("#mg_modal_loading").modal("hide");
}

MG.Global = {};

MG.Global.AlertMessage = function (message, title, call_back) {
    if (typeof (title) == "undefined") title = "Alert";
    $("<div></div>").html(message).dialog({
        modal: true,
        title: title,
        resizable: false,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
                if(typeof (call_back) == 'function'){
                    call_back()
                }
            }
        }
    });
}
