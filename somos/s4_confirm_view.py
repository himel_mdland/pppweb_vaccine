from datetime import datetime, timedelta
from urllib import parse
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.utils.dbconfig import sql_ec_db
from somos.biz.gmap_utils import GmapUtils
from somos.biz.apt_notify_biz import AptNotifyBiz


@csrf_exempt
def index(request):
    page_dict = dict({'ResultCode': 0})
    smg = SessionManager(request)
    page_dict['pat_id'] = smg.session.PatientID
    app_id = RequestUtils.get_string(request, 'appid')
    qid = RequestUtils.get_string(request, 'qid')
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)

    print(f's4_confimr_view, apt_id={app_id},clinic_id={clinic_id}')
    apt_notify_biz = AptNotifyBiz()
    apt_info = apt_notify_biz.load_appointment(app_id, clinic_id)
    page_dict['AppointmentID'] = app_id
    if apt_info is not None:
        page_dict['Lang'] = smg.get_lang_dict()
        page_dict['PatientName'] = apt_info["PatientName"]
        page_dict['ClinicID'] = apt_info["ClinicID"]
        page_dict['PatientID'] = apt_info["PatientID"]
        page_dict['LocationName'] = apt_info['LocationName']
        page_dict['LocationPhone'] = apt_info['LocationPhone']
        page_dict['LocationAddress'] = apt_info['LocationAddress']
        page_dict['LocationCity'] = apt_info['LocationCity']
        page_dict['LocationState'] = apt_info['LocationState']
        page_dict['LocationZip'] = apt_info['LocationZip']
        page_dict['PatientEmail'] = apt_info['PatientEmail']

        app_dt = datetime.strptime(str(apt_info['AppointmentDate']), '%Y-%m-%d %H:%M:%S')

        isofrom = app_dt.strftime("%Y%m%dT%H%M%S")
        isoto = (app_dt + timedelta(hours=1)).strftime("%Y%m%dT%H%M%S")
        calendar = "https://calendar.google.com/calendar/r/eventedit?text=My+COVID+Test&dates={isofrom}/{isoto}&details=Covid+19+RNA+or+antiBody+testing+at+{sitename}&location={address}&ctz=America/New_York".format(
                address=parse.quote_plus("+".join([apt_info['LocationAddress'], apt_info['LocationCity'], apt_info['LocationState'], apt_info['LocationZip']])),
                isoto=isoto,
                isofrom=isofrom,
                sitename=parse.quote_plus(apt_info['LocationName'] or "testing center.")
        )

        page_dict['AppointmentDate'] = app_dt.strftime('%B %d %Y')
        page_dict['AppointmentTime'] = app_dt.strftime('%A %I:%M %p')
        page_dict['Cal'] = calendar
        page_dict['Map'] = GmapUtils.gmap_img_from_cache(apt_info['LocationAddress'], apt_info['LocationCity'], apt_info['LocationState'], apt_info['LocationZip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")

        if int(app_id) > 1001627318: # do not notify history data
            apt_notify_biz.send_appointment_sms_notify(apt_info=apt_info)

        page_dict['request'] = request
        i18n = request.GET.get('i18n', None)
        page_dict['i18n'] = i18n
        return render(request, 'somos/confirm%s.html' % (("_" + i18n.upper()) if i18n else ''), page_dict)
    else:
        page_dict['ResultCode'] = 1 # appintment is not found
        return render(request, 'somos/error_page.html', page_dict)

@csrf_exempt
def password(request):
    page_dict = dict({'ResultCode': 0})
    page_dict['email'] = RequestUtils.get_string(request, 'email')
    return render(request, 'somos/password.html',page_dict)

