import re, datetime
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.common import DateUtils, RequestUtils, get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db
from somos.biz.apt_notify_biz import AptNotifyBiz
from somos.biz.apt_secure_biz import AptSecureBiz


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action is None or action == "":
        return init_html_page(request)
    elif action == 'searchApt':
        return search_apt(request)
    elif action == 'cancelApt':
        return cancel_apt(request)


def init_html_page(request):
    page_dict = dict()
    return render(request, 'somos/apt_cancel.html', page_dict)


def search_apt(request):
    res = dict({'AptID': -1})
    first_name = RequestUtils.get_string(request, 'FirstName')
    last_name = RequestUtils.get_string(request, 'LastName')
    dob = RequestUtils.get_date(request, 'DOB')
    zip_code = RequestUtils.get_string(request, 'Zipcode')
    apt_number = RequestUtils.get_long(request, 'AptNumber')
    search_for = RequestUtils.get_string(request, 'searchFor') # cancel, reschedule

    '''
    first_name = 'Eugene'
    last_name = 'Hu'
    dob = DateUtils.string_to_datetime('07/23/1987')
    zip_code = '10016'
    apt_number = 1002023512
    '''
    if dob is None or not isinstance(dob, datetime.datetime):
        res['Message'] = 'Invalid DOB'
    elif apt_number <= 0:
        res['Message'] = 'Invalid Appointment Number'
    elif first_name == '' or last_name == '' or zip_code == '':
        res['Message'] = 'First name, Last name and ZIP code is required.'
    else:
        apt_info = AptNotifyBiz().find_appointment(first_name, last_name, dob, zip_code, apt_number)
        for key in apt_info:
            res[key] = apt_info[key]
        res['OperationToken'] = AptSecureBiz().create_token_for_appointment(apt_id=apt_number, operation=search_for)
    return JsonResponse(res)


def cancel_apt(request):
    res_dict = dict({'ResultCode': 0})
    token = RequestUtils.get_string(request, 'token')
    apt_number = RequestUtils.get_long(request, 'AptNumber')
    zw = get_PHM_db()
    token_vo = zw['vac_token'].find_one({'Token': token})
    if token_vo is None or token_vo['Valid'] == 0 or token_vo['Operation'] != 'cancel' or apt_number != token_vo['AppointmentID']:
        res_dict = dict({'ResultCode': 1, 'Message': 'Invalid Token.'})
        return JsonResponse(res_dict)

    apt_id = token_vo['AppointmentID']
    ec_db = sql_ec_db(as_dict=True)
    apt_status = ec_db.query_first_row_to_dict("  select Status, BeginDateTime from appointment where id={0}".format(apt_id))
    if apt_status is None or apt_status.get('Status') not in (0, 6):
        res_dict = dict({'ResultCode': 2, 'Message': 'Appointment status is not "NEW", you are not allowed to CANCEL it.'})
        return JsonResponse(res_dict)
    elif apt_status.get('BeginDateTime') < datetime.datetime.now():
        res_dict = dict({'ResultCode': 3, 'Message': 'Appointment expired, you do not need to cancel it.'})
        return JsonResponse(res_dict)
    else:
        ec_db.excute_update('update appointment set status=3 where id={0}'.format(apt_id))
        zw['vac_token'].update_one({'Token': token}, {'$set': {'Valid': 0, 'EffectFlag': 1, 'TakeEffectTime': datetime.datetime.now()}})

    return JsonResponse(res_dict)
