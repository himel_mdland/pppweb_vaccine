import re, datetime
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.common import DateUtils, RequestUtils, get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.dbmodel.DbPhmLogRestAPI import DbPhmLogRestAPI
from PhmWeb.dbmodel.ic_covid_visitor import IcCovidVistor
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire
from PhmWeb.dbmodel.ic_patient import IcPatient
from somos.employee_slot import SlotDataService
from somos.biz.covid_booking_biz import CovidBookingBiz
from somos.biz.slot_utils import SlotUtils
from somos.biz.apt_notify_biz import AptNotifyBiz
from somos.s_recover import RecoverAccount


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    qid = RequestUtils.get_string(request, 'qid')
    if qid is not None and len(qid) > 60:
        return JsonResponse({'Status': 'injections protection!'})

    if action == 'searchProvider':
        return search_provider(request)

    qid = RequestUtils.get_string(request, 'qid')
    ques_biz = IcCovidRegQuestionnaire()
    ques_info = ques_biz.select_booking_info(qid)
    if ques_info is None:
        return redirect('/p/covid/')

    if action == 'saveBooking':
        return save_booking(request, ques_info)
    else:
        return init_page(request, ques_info)


def init_page(request, ques_info):
    print(f'ques_info={ques_info}')
    page_dict = dict()
    smg = SessionManager(request)
    page_dict['Lang'] = smg.get_lang_dict()
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)

    book_DT = ques_info['BookingPeriod']
    date_str = ''
    time_str = ''
    if isinstance(book_DT, datetime.datetime):
        date_str = book_DT.strftime('%A, %B %d')
        time_str = book_DT.strftime('%I:%M %p')

    LocationName = ''
    LocationInfo = ''
    LocationTestType = '' # 11/24/2020 Show Testing for
    LocationSeniorOnly = '' # 11/24/2020 Show Testing for
    location_id = int(ques_info['BookingLocationID'])
    ret = get_location_info(clinic_id, location_id)
    if ret is not None:
        LocationName = ret[0]['LocationName']
        LocationInfo = '{0}, {1}, {2}, {3}'.format(ret[0]['LocationAddress'], ret[0]['LocationCity'], ret[0]['LocationState'], ret[0]['LocationZip'])
        LocationTestType = ret[0]['TestType']
        LocationSeniorOnly = ret[0]['SeniorOnly']

    page_dict['AppointmentDate'] = date_str
    page_dict['AppointmentTime'] = time_str

    page_dict['LocationName'] = LocationName
    page_dict['LocationInfo'] = LocationInfo
    page_dict['LocationTestType'] = LocationTestType
    page_dict['LocationSeniorOnly'] = LocationSeniorOnly

    page_dict['HoldingSeconds'] = 300 - (datetime.datetime.now() - ques_info['BookingLockDate']).seconds # holding for 300s

    page_dict['SiteMode'] = ques_info.get('SiteMode')
    page_dict['qid'] = RequestUtils.get_string(request, 'qid')

    pat_id = smg.session.PatientID
    if pat_id > 0:
        pat_crud = PatientCrud(request)
        page_dict['Pat'] = pat_crud.get_patient_edit_info(pat_id)
    page_dict['request'] = request
    i18n = request.GET.get('i18n', None)
    page_dict['i18n'] = i18n
    return render(request, 'somos/register%s.html' % (("_"+i18n.upper()) if i18n else ''), page_dict)


def save_booking(request, ques_info):
    res_dict = dict()
    error_msg = ''
    appid = 0
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    smg = SessionManager(request)
    bookBiz = CovidBookingBiz()
    visitBiz = IcCovidVistor()
    quesBiz = IcCovidRegQuestionnaire()
    qid = ques_info['QID']
    location_id = ques_info['BookingLocationID']
    # 5/29/2020 avalidation for booking, when slot is due, and some registered the same slot
    slot_service = SlotDataService(clinic_id=clinic_id)
    if False and slot_service.is_available_to_book(location=ques_info['BookingLocationID'], book_time=ques_info['BookingPeriod'], my_locker=qid):
        res_dict['ResultCode'] = 1
        res_dict['Errors'] = "Your selected slot is not available, please go back to selected another time slot."
        res_dict['HasError'] = True
        return JsonResponse(res_dict)
    validate_zipcode = SlotUtils.is_valid_zipcode(location_id=location_id, zipcode=RequestUtils.get_safe_sql_string(request, 'PatientAddrZip'))
    if validate_zipcode > 0:
        res_dict['ResultType'] = 'Zipcode Invalid'
        res_dict['ResultCode'] = 1
        res_dict['HasError'] = True
        res_dict['ErrorsCode'] = validate_zipcode

        if validate_zipcode == 2:
            zip_list = SlotUtils.get_olid_zip_dict()[location_id]
            if zip_list:
                res_dict['Errors'] = "This location is for residents in zip codes %s only. Proof of residency will be requested upon check in. Thank you." % ", ".join(zip_list)
            else:
                res_dict['Errors'] = "This location is currently under maintenance (Code: ZV), please come back later!"
                print("WARNING: YOU SET ZIPCODEVALIDAION TO YES FOR SITE, BUT DIDN'T ADD ROWS TO ZIPVALIDATION TABLE!")
        else:
            res_dict['Errors'] = "Your zipcode does not appear to be valid, please correct the zipcode and try again."

        # save log to mongdb
        log_dict = dict({'LogCode': 'pppvac_invalid'})
        log_dict.update(res_dict)
        log_dict['Qinfo'] = ques_info
        DbPhmLogRestAPI.instance().add_log(log_dict, request)

        return JsonResponse(res_dict)

    validate_slot_result_code = SlotUtils.is_able_to_commit_appointment(location_id=location_id, book_time=ques_info['BookingPeriod'])
    if validate_slot_result_code > 0:
        res_dict['ResultType'] = 'Sot Unavailable'
        res_dict['ResultCode'] = 1
        res_dict['Errors'] = "Your selected slot is not available, please go back to selected another time slot."
        res_dict['HasError'] = True
        res_dict['ErrorsCode'] = validate_slot_result_code

        # save log to mongdb
        log_dict = dict({'LogCode': 'pppvac_invalid'})
        log_dict.update(res_dict)
        log_dict['Qinfo'] = ques_info
        DbPhmLogRestAPI.instance().add_log(log_dict, request)
        return JsonResponse(res_dict)

    # validate dob
    patient_dob = RequestUtils.get_string(request, 'PatientDOB')
    if not CovidBookingBiz.is_valid_dob(patient_dob):
        res_dict['ResultType'] = 'Invalid DOB'
        res_dict['ResultCode'] = 1
        res_dict['Errors'] = "Your DOB not valid, please check."
        res_dict['HasError'] = True
        res_dict['ErrorsCode'] = validate_slot_result_code
        return JsonResponse(res_dict)

    visitor_id = bookBiz.find_existing_visitor(clinic_id, request)
    if visitor_id == 0:
        visitor_id = visitBiz.register_visitor(request, clinic_id)
        quesBiz.update_visitor_id(qid, visitor_id)

    if visitor_id > 0:
        patient_id = smg.session.PatientID # from session
        if patient_id > 0:
            ic_pat = IcPatient()
            ic_pat.update_patient(request, patient_id=patient_id, clinic_id=clinic_id)
        else:  # try match by name and dob
            patient_id = bookBiz.find_existing_patient(clinic_id=clinic_id, request=request)

        if patient_id <= 0: # insert to db by creating new patient
            patient_id = bookBiz.register_patient(clinic_id, visitor_id, request)
            visitBiz.update_patient_id(clinic_id, visitor_id, patient_id)
            quesBiz.update_patient_id(qid, patient_id)

        if patient_id > 0: # it is true here, otherwise it is error
            app_id = bookBiz.find_existing_appointment(clinic_id, patient_id)
            if app_id > 0: # this patient already has future apponintment
                error_msg = AptNotifyBiz().print_appointment_html(clinic_id=clinic_id, apt_id=app_id)
            else:
                app_id = bookBiz.add_appointment(clinic_id, patient_id, request, ques_info)
                i_understand_testingfor = RequestUtils.get_string(request, 'iUnderstandTestingfor')
                quesBiz.update_appointment_id(qid, app_id, i_understand_testingfor)
                appid = app_id

                # xiao: when booking is success, send welcome message
                patient_first_name = RequestUtils.get_safe_sql_string(request, 'PatientFirstName')
                email = RequestUtils.get_safe_sql_string(request, 'PatientEmail')
                #biz = RecoverAccount()
                #biz.email_welcome_message(patient_first_name, email, False)

    res_dict['Errors'] = error_msg
    res_dict['appid'] = appid
    res_dict['HasError'] = len(error_msg) > 0
    res_dict['qid'] = RequestUtils.get_string(request, 'qid')
    print(f'save_booking res_dict={res_dict}')
    return JsonResponse(res_dict)


def search_provider(request):
    res_dict = dict()

    html = ''
    hasProvider = '0'
    search_str = RequestUtils.get_string(request, 'searchStr')
    provider_list = get_somos_provider(search_str)
    if provider_list is not None:
        for p in provider_list:
            html += "<option title=\"{3} ({4})\" value='{0}'>{1}, {2}</option>".format(p['Phone'], p['Provider Last Name'], p['Provider First Name'], p['Practice Name'].replace('"',"'"), p['Zip'])

    if len(html)> 0:
        hasProvider = '1'

    res_dict['HasMatchProvider'] = hasProvider
    res_dict['ProviderListHTML'] = html
    return JsonResponse(res_dict)


def get_location_info(clinic_id, location_id):
    sql = "select l.Name LocationName, l.Address LocationAddress, l.City LocationCity, l.State LocationState, l.Zip LocationZip, l.Phone LocationPhone,l.TestType, l.SeniorOnly, l.ZipcodeValidation from Ref_ClinicOfficeLocation l where l.OLID = '{0}' and l.ClinicID='{1}'"\
        .format(location_id, clinic_id)
    
    ec_db = sql_ec_db(as_dict=True)
    ret = ec_db.excute_select(sql)
    ec_db.close_conn()

    if len(ret) > 0:
        return ret
    return None


def get_somos_provider(search_str):
    provider_list = get_PHM_db().somosproviders.find({"$or": [{"Provider First Name": re.compile(search_str, re.IGNORECASE),
                                                               "Provider Last Name": re.compile(search_str, re.IGNORECASE),
                                                               #"Zip" : re.compile(search_str, re.IGNORECASE),
                                                               }]}).limit(20)
    if provider_list is not None:
        return provider_list
    return None


class PatientCrud:
    def __init__(self, request):
        self._req = request
        self._ec_db_util = sql_ec_db(as_dict=True)


    def get_patient_edit_info(self, patient_id):
        # sql = f"""select pat.* from Patient pat where pat.PatientID='{patient_id}'"""
        sql = f"""select pat.*, ISNULL(apt.OrderID, -1) OrderID, ISNULL(apt.RequisitionNumber, '') RequisitionNumber ,apt.Status as AppointmentStatus,
                    dbo.fn_mongo_GetPatientRaceName(pat.Clinicid, pat.PatientID) PatientRaceName,  dbo.fn_mongo_GetPatientRaceCode(pat.Clinicid, pat.PatientID) PatientRaceCode, 
                    dbo.fn_mongo_GetPatientEthnicityName(pat.Clinicid, pat.PatientID) PatientEthnicityName, 
                    dbo.fn_mongo_GetPatientEthnicityCode(pat.Clinicid, pat.PatientID) PatientEthnicityCode
                           from Patient pat left join Appointment apt on pat.PatientID = apt.PatientID where """

        sql += f"  pat.PatientID='{patient_id}'"

        patient_vo = self._ec_db_util.query_first_row_to_dict(sql)

        if 'PatientMemo' not in patient_vo or patient_vo['PatientMemo'] is None:
            patient_vo['PatientMemo'] = ''
        for key in patient_vo:
            vl = patient_vo[key]
            if vl == 'none' or vl is None:
                patient_vo[key] = ''
            elif isinstance(vl, str):
                patient_vo[key] = vl.strip()
        patient_vo['PatientDOB'] = DateUtils.datetime_to_short_string(patient_vo.get('PatientDOB'))
        return patient_vo


