import re, datetime
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.common import DateUtils, RequestUtils, get_PHM_db
from PhmWeb.utils.dbconfig import sql_ec_db


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action is None or action == "":
        return init_html_page(request)
    elif action == 'SaveReschedule':
        return save_reschedule(request)


def init_html_page(request):
    page_dict = dict()
    return render(request, 'somos/apt_reschedule.html', page_dict)


def save_reschedule(request):
    res_dict = dict({'ResultCode': 0, 'Message': ''})
    token = RequestUtils.get_string(request, 'token')
    empid = RequestUtils.get_long(request, 'EmpID')
    olid = RequestUtils.get_long(request, 'Olid')
    book_time = RequestUtils.get_string(request, 'BookTime')

    zw = get_PHM_db()
    token_vo = zw['vac_token'].find_one({'Token': token})
    if token_vo is None or token_vo['Valid'] == 0 or token_vo['Operation'] != 'reschedule':
        res_dict = dict({'ResultCode': 1, 'Message': 'Invalid Token.'})
        return JsonResponse(res_dict)
    apt_id = token_vo['AppointmentID']

    book_DT = datetime.datetime.strptime(book_time, "%m/%d/%Y %H:%M:%S")
    seg_DT = 0
    if isinstance(book_DT, datetime.datetime):
        seg_DT = int((book_DT.hour * 60 + book_DT.minute) / 5)

    sql_update = '''update appointment set BeginDateTime='{0}',EndDateTime='{0}',LocationID={1} , BeginTime={2}, EndTime={2}
                   where id='{3}' and status in(0, 6) 
                   '''.format(book_time, olid, seg_DT, apt_id)
    ec_db = sql_ec_db(as_dict=True)
    ec_db.excute_update(sql_update)

    up_vo = {'Valid': 0, 'EffectFlag': 1, 'TakeEffectTime': datetime.datetime.now()}
    up_vo['BookTime'] = book_time
    up_vo['Olid'] = olid
    up_vo['EmpID'] = empid
    zw['vac_token'].update_one({'Token': token}, {'$set': up_vo})
    return JsonResponse(res_dict)
