import datetime, json, bson
from collections import OrderedDict
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.templatetags.static import static
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.dbmodel.DbPhmLogRestAPI import DbPhmLogRestAPI
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire
from PhmWeb.settings import PHMSettings
from somos.employee_slot import EmployeeSlot, SlotDataService, SlotUtils
from somos.biz.gmap_utils import GmapUtils
from somos.biz.apt_secure_biz import AptSecureBiz
from collections import OrderedDict
from django.template.loader import get_template


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action == 'setSelectedSlot':
        return set_selected_slot(request)
    elif action == 'getSiteWithSlot':
        search_day = RequestUtils.get_string(request, 'day')
        step = RequestUtils.get_long(request, 'step')
        address = str(RequestUtils.get_string(request, 'zipcode').strip() or "11106")
        location_id = RequestUtils.get_long(request, 'locationID')  # edit appointment can not change site
        clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
        json = RequestUtils.get_long(request, 'json')
        vaccine_offered = RequestUtils.get_string(request, 'vaccineOffered')

        # 7/25/2022 Children Under 5 Vaccination
        children_under_5 = IcCovidRegQuestionnaire().is_vaccination_children_under_5(RequestUtils.get_string(request, 'qid'))
        if children_under_5:
            vaccine_offered = 'ChildrenUnder5'

        biz = SearchingSiteWithSlot()
        json_response = biz.get_site_with_slot(search_day, step, address, location_id, clinic_id, vaccine_offered, json)
        if biz.get_num_of_valid_sites() == 0:
            search_day = DateUtils.datetime_to_short_string(biz.get_search_day() + datetime.timedelta(days=1))
            json_response = biz.get_site_with_slot(search_day, step, address, location_id, clinic_id, vaccine_offered, json)
        return json_response
    else:
        return init_html_page(request)


def init_html_page(request):
    page_dict = dict()
    smg = SessionManager(request)
    page_dict['Lang'] = smg.get_lang_dict()
    page_dict['BookingDay'] = DateUtils.datetime_to_short_string(datetime.datetime.now())
    qid = RequestUtils.get_safe_sql_string(request, 'qid')
    page_dict['qid'] = qid
    page_dict['ValidQidCode'] = AptSecureBiz().validate_for_select_site(qid, request)
    page_dict['LocationID'] = RequestUtils.get_string(request, 'locationID')
    page_dict['request'] = request

    # 7/25/2022 Children Under 5 Vaccination
    children_under_5 = IcCovidRegQuestionnaire().is_vaccination_children_under_5(RequestUtils.get_string(request, 'qid'))
    if children_under_5:
        page_dict['SiteMode'] = 'ChildrenUnder5'
        
    i18n = request.GET.get('i18n', None)
    page_dict['i18n'] = i18n
    return render(request, 'somos/site_list%s.html' % (("_"+i18n.upper()) if i18n else ''), page_dict)


def set_selected_slot(request):
    res = dict({'ResultCode': 0, 'Errors': ''})
    emp_id = RequestUtils.get_long(request, 'EmpID')
    ol_id = RequestUtils.get_long(request, 'Olid')
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    book_time = RequestUtils.get_string(request, 'BookTime') # 05/31/2020 10:20:00
    qid = RequestUtils.get_string(request, 'qid')

    channel = RequestUtils.get_string(request, 'Channel') # phmweb_relocate -- not require to validate
    if channel == 'phmweb_relocate':
        biz = IcCovidRegQuestionnaire()
        biz.update_booking_info(qid, emp_id, ol_id, book_time)
        return JsonResponse(res)

    if qid == '':
        res['ResultCode'] = 1
        res['Errors'] = ['somos-appt-unavailable', 'somos-appt-invalid-qid']
        return JsonResponse(res)
    # 05/29202 add validation before locking
    slot_sevice = SlotDataService(clinic_id)
    dt_book_time = datetime.datetime.strptime(book_time, "%m/%d/%Y %H:%M:%S")
    if not slot_sevice.is_available_to_book(location=ol_id, book_time=dt_book_time, my_locker=qid):
        res['ResultCode'] = 1
        res['Errors'] = ['somos-appt-unavailable', 'somos-sites-already-booked']
        return JsonResponse(res)
    # validate questionnair
    valid_qid = AptSecureBiz().validate_for_select_site(qid, request)
    if valid_qid > 0:
        res['ResultCode'] = 1
        res['Errors'] = ['somos-appt-unavailable', 'somos-appt-invalid-qid']
        return JsonResponse(res)

    # double check
    validate_slot_result_code = SlotUtils.is_able_to_commit_appointment(location_id=ol_id, book_time=dt_book_time)
    if validate_slot_result_code > 0:
        res['ResultCode'] = 1
        res['ErrorsCode'] = validate_slot_result_code
        if validate_slot_result_code == 1:
            res['Errors'] = ['somos-appt-unavailable', 'This day is not available any more.']
        elif validate_slot_result_code == 2:
            res['Errors'] = ['somos-appt-unavailable', 'This day has reached its upper limit, please choose another day']
        else:
            res['Errors'] = ['somos-appt-unavailable', 'somos-sites-already-booked']

        # save log to mongdb
        log_dict = dict({'LogCode': 'pppvac_invalid'})
        log_dict.update(res)
        log_dict['Action'] = 'setSelectedSlot'
        log_dict['Qid'] = qid
        log_dict['LocationID'] = ol_id
        log_dict['ClinicID'] = clinic_id
        log_dict['BooKTime'] = book_time
        DbPhmLogRestAPI.instance().add_log(log_dict, request)

        return JsonResponse(res)

    biz = IcCovidRegQuestionnaire()
    biz.update_booking_info(qid, emp_id, ol_id, book_time)
    return JsonResponse(res)


class SearchingSiteWithSlot:

    def __init__(self):
        self._num_of_valid_sites = 0 # site has slot to make appointment
        self._dt_search_day = None

    def get_num_of_valid_sites(self):
        return self._num_of_valid_sites

    def get_search_day(self):
        return self._dt_search_day

    def get_site_with_slot(self, search_day, step, address, location_id, clinic_id, vaccine_offered, json):
        res_dict = dict()
        if search_day is None or search_day == '':
            search_day = DateUtils.datetime_to_short_string(datetime.datetime.now())
        dt_search_day = DateUtils.string_to_datetime(search_day)
        self._dt_search_day = dt_search_day

        if step != 0:
            dt_search_day = dt_search_day + datetime.timedelta(days=step)
            search_day = DateUtils.datetime_to_short_string(dt_search_day)

        res_dict['BookingDay'] = search_day

        res_dict["Prev"] = True
        if datetime.datetime.now().date() >= dt_search_day.date():
            res_dict["Prev"] = False

        # only search recent 50 days
        dt_delta = (dt_search_day - datetime.datetime.now()).days
        print(f'dt_search_day={dt_search_day}, dt_delta={dt_delta}')
        if dt_delta > 90 or dt_delta < -5:
            res_dict['Count'] = 0
            res_dict['Content'] = ''
            return JsonResponse(res_dict)

        location_name = ''

        slot_data_service = SlotDataService(clinic_id=clinic_id)
        emp_list = slot_data_service.get_em_with_location(location_id=location_id)
        slot_lock_dict = slot_data_service.get_in_lock_appoint(dt_search_day, dt_search_day + datetime.timedelta(days=6))  # all locations' appointment
        emp_vacation_dict = slot_data_service.get_employee_vacation(dt_search_day, dt_search_day + datetime.timedelta(days=6)) # all locations' vaction
        overnight_dict = slot_data_service.get_location_overnight_setting()
        # print(f'emp_list={emp_list}')

        # print(f'vaccine_offered={vaccine_offered}')
        if vaccine_offered == 'ChildrenUnder5':
            emp_list = [x for x in emp_list if x['OfficeLocation'].get('EligibleUnderAge5') is not None and x['OfficeLocation'].get('EligibleUnderAge5') == 1]
        elif vaccine_offered is not None and len(vaccine_offered) > 5:
            emp_list = [x for x in emp_list if 'OfficeLocation' in x and x.get('OfficeLocation') is not None]

            emp_list = [x for x in emp_list if
                           (vaccine_offered.find('Johnson18') >= 0 and x['OfficeLocation'].get('Johnson18') is not None and x['OfficeLocation'].get('Johnson18') == 1)
                        or (vaccine_offered.find('Moderna18') >= 0 and x['OfficeLocation'].get('Moderna18') is not None and x['OfficeLocation'].get('Moderna18') == 1)
                        or (vaccine_offered.find('Pfizer12') >= 0 and x['OfficeLocation'].get('Pfizer12') is not None and x['OfficeLocation'].get('Pfizer12') == 1)
                        or (vaccine_offered.find('Pfizer0511') >= 0 and x['OfficeLocation'].get('Pfizer0511') is not None and x['OfficeLocation'].get('Pfizer0511') == 1)
                        ]

        distance_res = slot_data_service.fill_em_with_distance(emp_list, address)
        if distance_res['ResultCode'] > 0:
            return JsonResponse(distance_res)

        emp_gmap_list = distance_res['EmpList']

        if json == 1:
            res_dict['emp_gmap_list'] = emp_gmap_list
            res_dict['slot_lock_dict'] = slot_lock_dict
            res_dict['emp_vacation_dict'] = emp_vacation_dict
            res_dict['LocationName'] = location_name
            res_dict['GAddr'] = distance_res['Result']["origin_addresses"][0] if 'origin_addresses' in distance_res['Result'] else address
            return res_dict

        mc, num_of_sites, self._num_of_valid_sites = self.render_to_pppweb(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, overnight_dict) # pp.mdland.com
        if num_of_sites == 0:
            mc = self.render_to_pppweb_na(emp_gmap_list, search_day)

        res_dict['Content'] = mc
        res_dict['Count'] = num_of_sites
        res_dict['LocationName'] = location_name
        res_dict['GAddr'] = distance_res['Result']["origin_addresses"][0] if 'origin_addresses' in distance_res['Result'] else address
        return JsonResponse(res_dict)


    def render_to_pppweb(self, emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, overnight_dict):
        mc = ''
        num_of_sites = 0
        num_of_valid_sites = 0

        for emp in emp_gmap_list:
            num_of_sites += 1

            empid = emp['EmployeeID']
            ol = emp['OfficeLocation']
            olid = ol['OLID']

            emp_slot = EmployeeSlot(period_days=5)
            emp_slot.set_week_schedule(emp)  #
            emp_slot.set_inlock_appoint_dict(slot_lock_dict)
            emp_slot.set_employee_vacation(emp_vacation_dict)
            emp_slot.set_overnight_site(overnight_dict)
            slot_interval = SlotUtils.get_schedule_period_by_location(location_id=ol['OLID'])
            day_list = emp_slot.get_slot_by_period(location_id=ol['OLID'], day_begin=search_day, slot_interval=slot_interval)
            #if len(day_list) > 0: continue # all slot of the location are empty

            tt = self.__render_first_day_buttons(day_list, search_day, empid, olid)
            if len(day_list) > 0 and tt is not None:
                num_of_valid_sites += 1

            if PHMSettings.DISABLE_GMAPS_CACHE:
                map = GmapUtils.gmap_img(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
            else:
                map = GmapUtils.gmap_img_from_cache(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")

            senior_only = '<div class="text-danger Gotham-book g-font-size-16">Currently, this site is seeing seniors <b>65+ only</b>.</div>' if ol['SeniorOnly'] else ''
            temp_only = '<div class="text-danger Gotham-book  g-font-size-16">All vaccine appointments between 9:00am and 12:00pm from 2/12 to 2/21 are ONLY for DOE staff from D75, elementary, and middle schools who are scheduled to work in-person.</div>' if str(olid) in ("1367", "1368", "1369") else ""
            print("temp:%s"%temp_only)
            subtitle = "<p class='text-muted' style='font-size: smaller;'>%s</p>" % ol["SiteSubTitle"] if ol.get("SiteSubTitle", None) else ''

            Johnson18 = ol.get('Johnson18') is not None and ol.get('Johnson18') == 1
            Moderna18 = ol.get('Moderna18') is not None and ol.get('Moderna18') == 1
            Pfizer12 = ol.get('Pfizer12') is not None and ol.get('Pfizer12') == 1
            Pfizer0511 = ol.get('Pfizer0511') is not None and ol.get('Pfizer0511') == 1
            Pfizer05 = ol.get('Pfizer05') is not None and ol.get('Pfizer05') == 1
            Moderna05 = ol.get('Moderna05') is not None and ol.get('Moderna05') == 1
            if Johnson18 or Moderna18 or Pfizer12 or Pfizer0511 or Pfizer05 or Moderna05:
                temp_only += '<div class="text- Gotham-book  g-font-size-16"> Vaccines offered: '
                if Johnson18:
                    temp_only += '<span class= >√</span> Johnson & Johnson (18+)&nbsp;&nbsp;'
                if Moderna18:
                    temp_only += '<span class= >√</span> Moderna (18+)&nbsp;&nbsp;'
                if Pfizer12:
                    temp_only += '<span class= >√</span> Pfizer (12+)&nbsp;&nbsp;'
                if Pfizer0511:
                    temp_only += '<span class= >√</span> Pfizer (5-11)&nbsp;&nbsp;'
                if Pfizer05:
                    temp_only += '<span class= >√</span> Pfizer (Under 5)&nbsp;&nbsp;'
                if Moderna05:
                    temp_only += '<span class= >√</span> Moderna (Under 5)&nbsp;&nbsp;'
                temp_only += '</div>'

            EligibleUnderAge5 = ol.get('EligibleUnderAge5') is not None and ol.get('EligibleUnderAge5') == 1
            if EligibleUnderAge5:
                temp_only += "<div class='text-Gotham-book  g-font-size-16' style='color:#3398dc'>Vaccinating children under 5 </div>"
            restricted_zip = ""
            restricted_zip_instructions = ""
            olid_zip_enforced = olid in SlotUtils.get_zip_enforced_olids()
            zips_for_olid = SlotUtils.get_olid_zip_dict()[olid]

            if olid_zip_enforced and zips_for_olid:
                restricted_zip = "This location is only for residents in the following zipcodes:"
                restricted_zip += "<br>%s" % ", ".join(zips_for_olid)
                restricted_zip = "<p class='text-muted' style='font-size: smaller;'>%s</p>" % restricted_zip
                restricted_zip_instructions = '<div class="text-danger Gotham-book  g-font-size-16">Please bring the following as proof of residency: <ul><li>One of the following: State or government-issued ID; Statement from landlord; Current rent receipt or lease; Mortgage records; or</li><li>Two of the following: Statement from another person; Current mail; School records. </li></div>'

            mc += '<div class="row mb-3 pb-5 pt-5 appointment_card">'

            mc += '<div class="col-lg-12 col-md-12 col-sm-12">'
            mc += '<h5 class="Gotham-black g-font-size-25" id="divLocationName{1}">{0}</h5>'.format(ol['Name'], olid)
            mc += f'{subtitle}'
            mc += f'{restricted_zip}'

            if olid is not None and olid == 1622:
                map = '/p/static/img/maps/somos-site-1622-map.png'
            else:
                mc += '<p class="p-secd-font" id="divLocationAddr{4}"><b>{0}, {1}, {2} - {3}</b></p>'.format(ol['Address'], ol['City'], ol['State'], ol['Zip'], olid)

            mc += '<div class="row g-mx-2">'

            #mc += '''<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 g-bg-img-hero g-min-height-50 d-none d-md-block map-display" style="min-width: 308px;background: url('%s' ) center no-repeat, url('%s') center no-repeat;"></div>''' % (static('images/somos-emblem-marker.png'), map)

            mc += '<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 info-height2 g-px-20--lg g-px-20--xl">'

            mc += '<div class="d-none d-sm-block pb-2 color-change">'
            #mc += '<div class="p-secd-font"><img src="/p/static/images/call@2x.png" class="g-width-25" alt="Apple"> <span>{0}</span></div>'.format(ol['Phone'])
            #mc += f'''<div class="p-secd-font"><img src="/p/static/images/icon_location@2x.png" class="g-width-25" alt="Location"> {emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''} away</div>'''
            mc += '</div>'

            mc += '<div class="d-sm-none row pb-2 color-change">'
            mc += '<div class="p-secd-font col-7" style="white-space: nowrap;"><img src="/p/static/images/call@2x.png" class="g-width-25" alt="Apple"> <span>{0}</span></div>'.format(ol['Phone'])
            mc += f'''<div class="p-secd-font col-5 text-center" style="white-space: nowrap;"><img src="/p/static/images/icon_location@2x.png" class="g-width-25" alt="Location"> {emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''} away</div>'''
            mc += '</div>'

            mc += f'{senior_only}{temp_only}{restricted_zip_instructions}'

            mc += '</div>'
            mc += '</div>'
            mc += '</div>'

            mc += (tt if tt is not None else '<div style="margin-left:20px">NA</div>')

            mc += '</div></div>'
            mc += '</div>'

        return mc, num_of_sites, num_of_valid_sites


    def __render_first_day_buttons(self, day_list, search_day, empid, olid):
        dt_search_day = DateUtils.string_to_datetime(search_day)
        valid_slot_list = [x for x in day_list if x['Date'] == dt_search_day]
        tt = ''
        if len(valid_slot_list) == 0:
            return None

        day = valid_slot_list[0]
        if len(day['SlotList']) == 0:
            return None

        for slot in day['SlotList']:
            tt += f'''<button class="appointment-slot btn btn-sm u-btn-outline-blue" style="margin:5px; background-color2:#65b392;" onclick="selectSlot({empid},{olid},'{slot['Value']}', '{slot['Name']}')" >{slot['Name']}</button>'''
        return tt

    def render_to_pppweb_na(self, emp_gmap_list, search_day):
        mc = ''
        for ol_index, emp in enumerate(emp_gmap_list):
            empid = emp['EmployeeID']
            ol = emp['OfficeLocation']
            olid = ol['OLID']

            senior_only = '<div class="text-danger Gotham-book g-font-size-16">Currently, this site is seeing seniors <b>65+ only</b>.</div>' if ol['SeniorOnly'] else ''

            restricted_zip = ""
            restricted_zip_instructions = ""
            olid_zip_enforced = olid in SlotUtils.get_zip_enforced_olids()
            zips_for_olid = SlotUtils.get_olid_zip_dict()[olid]

            if olid_zip_enforced and zips_for_olid:
                restricted_zip = "This location is only for residents in the following zipcodes:"
                restricted_zip += "<br>%s" % ", ".join(zips_for_olid)
                restricted_zip = "<p class='text-muted' style='font-size: smaller;'>%s</p>" % restricted_zip
                restricted_zip_instructions = '<div class="text-danger Gotham-book  g-font-size-16">Please bring the following as proof of residency: <ul><li>One of the following: State or government-issued ID; Statement from landlord; Current rent receipt or lease; Mortgage records; or</li><li>Two of the following: Statement from another person; Current mail; School records. </li></div>'

            mc += '<div class="row mb-3 pb-1 pt-2 appointment_card">'

            mc += '<div class="col-lg-12 col-md-11 col-sm-12">'
            mc += '<h5 class="Gotham-black g-font-size-25">{0}</h5>'.format(ol['Name'])
            # mc += f'{restricted_zip}‘

            mc += '<p class="p-secd-font"><b>{0}, {1}, {2} - {3}</b></p>'.format(ol['Address'], ol['City'], ol['State'], ol['Zip'])

            mc += '<div class="row g-mx-2">'
            mc += '<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12   g-px-20--lg g-px-20--xl">'

            mc += '<div class="d-none d-sm-block pb-2 color-change">'
            mc += '</div>'

            mc += '<div class="d-sm-none row pb-2 color-change">'
            mc += '<div class="p-secd-font col-7" style="white-space: nowrap;"><img src="/p/static/images/call@2x.png" class="g-width-25" alt="Apple"> <span>{0}</span></div>'.format(ol['Phone'])
            mc += f'''<div class="p-secd-font col-5 text-center" style="white-space: nowrap;"><img src="/p/static/images/icon_location@2x.png" class="g-width-25" alt="Location"> {emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''} away</div>'''
            mc += '</div>'

            mc += f'{senior_only}{restricted_zip_instructions}'
            mc += 'NA'
            mc += '</div>'
            mc += '</div>'
            mc += '</div>'

            mc += '</div>'

        return mc


# for https://somosvaccination.mdland.com/  https://rendrcare.mdland.com
def render_to_pppweb_old(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, overnight_dict):
    mc = ''
    num_of_sites = 0
    for emp in emp_gmap_list:
        empid = emp['EmployeeID']
        ol = emp['OfficeLocation']
        olid = ol['OLID']

        emp_slot = EmployeeSlot(period_days=5)
        emp_slot.set_week_schedule(emp)  #
        emp_slot.set_inlock_appoint_dict(slot_lock_dict)
        emp_slot.set_employee_vacation(emp_vacation_dict)
        emp_slot.set_overnight_site(overnight_dict)
        slot_interval = SlotUtils.get_schedule_period_by_location(location_id=ol['OLID'])
        day_list = emp_slot.get_slot_by_period(location_id=ol['OLID'], day_begin=search_day, slot_interval=slot_interval)
        if len(day_list) == 0: # all slot of the location are empty
            continue
        num_of_sites += 1
        if PHMSettings.DISABLE_GMAPS_CACHE:
            map = GmapUtils.gmap_img(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        else:
            map = GmapUtils.gmap_img_from_cache(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")

        senior_only = '<div class="text-danger Gotham-book g-font-size-16">Currently, this site is seeing seniors <b>65+ only</b>.</div>' if ol['SeniorOnly'] else ''
        temp_only = '<div class="text-danger Gotham-book  g-font-size-16">All vaccine appointments between 9:00am and 12:00pm from 2/12 to 2/21 are ONLY for DOE staff from D75, elementary, and middle schools who are scheduled to work in-person.</div>' if str(olid) in ("1367", "1368", "1369") else ""
        print("temp:%s"%temp_only)
        subtitle = "<p class='text-muted' style='font-size: smaller;'>%s</p>" % ol["SiteSubTitle"] if ol.get("SiteSubTitle", None) else ''

        Johnson18 = ol.get('Johnson18') is not None and ol.get('Johnson18') == 1
        Moderna18 = ol.get('Moderna18') is not None and ol.get('Moderna18') == 1
        Pfizer12 = ol.get('Pfizer12') is not None and ol.get('Pfizer12') == 1
        Pfizer0511 = ol.get('Pfizer0511') is not None and ol.get('Pfizer0511') == 1
        if Johnson18 or Moderna18 or Pfizer12 or Pfizer0511:
            temp_only += '<div class="text- Gotham-book  g-font-size-16"> Vaccines offered: '
            if Johnson18:
                temp_only += '<span class= >√</span> Johnson & Johnson (18+)&nbsp;&nbsp;'
            if Moderna18:
                temp_only += '<span class= >√</span> Moderna (18+)&nbsp;&nbsp;'
            if Pfizer12:
                temp_only += '<span class= >√</span> Pfizer (12+)&nbsp;&nbsp;'
            if Pfizer0511:
                temp_only += '<span class= >√</span> Pfizer (5-11)'
            temp_only += '</div>'

        restricted_zip = ""
        restricted_zip_instructions = ""
        olid_zip_enforced = olid in SlotUtils.get_zip_enforced_olids()
        zips_for_olid = SlotUtils.get_olid_zip_dict()[olid]

        if olid_zip_enforced and zips_for_olid:
            restricted_zip = "This location is only for residents in the following zipcodes:"
            restricted_zip += "<br>%s" % ", ".join(zips_for_olid)
            restricted_zip = "<p class='text-muted' style='font-size: smaller;'>%s</p>" % restricted_zip
            restricted_zip_instructions = '<div class="text-danger Gotham-book  g-font-size-16">Please bring the following as proof of residency: <ul><li>One of the following: State or government-issued ID; Statement from landlord; Current rent receipt or lease; Mortgage records; or</li><li>Two of the following: Statement from another person; Current mail; School records. </li></div>'


        mc += '<div class="row mb-3 pb-5 pt-5">'

        mc += '<div class="col-lg-7 col-md-6 col-sm-12">'
        mc += '<h5 class="Gotham-black g-font-size-25">{0}</h5>'.format(ol['Name'])
        mc += f'{subtitle}'
        mc += f'{restricted_zip}'

        if olid is not None and olid == 1622:
            map = '/p/static/img/maps/somos-site-1622-map.png'
        else:
            mc += '<p class="p-secd-font"><b>{0}, {1}, {2} - {3}</b></p>'.format(ol['Address'], ol['City'], ol['State'], ol['Zip'])

        mc += '<div class="row g-mx-2">'

        mc += '''<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 g-bg-img-hero g-min-height-50 d-none d-md-block map-display" style="min-width: 308px;background: url('%s' ) center no-repeat, url('%s') center no-repeat;"></div>''' % (static('images/somos-emblem-marker.png'), map)

        mc += '<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 info-height g-px-20--lg g-px-20--xl">'

        mc += '<div class="d-none d-sm-block pb-2 color-change">'
        mc += '<div class="p-secd-font"><img src="/p/static/images/call@2x.png" class="g-width-25" alt="Apple"> <span>{0}</span></div>'.format(ol['Phone'])
        mc += f'''<div class="p-secd-font"><img src="/p/static/images/icon_location@2x.png" class="g-width-25" alt="Location"> {emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''} away</div>'''
        mc += '</div>'

        mc += '<div class="d-sm-none row pb-2 color-change">'
        mc += '<div class="p-secd-font col-7" style="white-space: nowrap;"><img src="/p/static/images/call@2x.png" class="g-width-25" alt="Apple"> <span>{0}</span></div>'.format(ol['Phone'])
        mc += f'''<div class="p-secd-font col-5 text-center" style="white-space: nowrap;"><img src="/p/static/images/icon_location@2x.png" class="g-width-25" alt="Location"> {emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''} away</div>'''
        mc += '</div>'

        mc += f'{senior_only}{temp_only}{restricted_zip_instructions}'

        mc += '</div>'
        mc += '</div>'
        mc += '</div>'

        mc += '<div class="col-lg-5 col-md-6 col-sm-12">'
        days_dict = OrderedDict()
        for i, day in enumerate(day_list):
            slot_list = []
            for slot in day['SlotList']:
                slot_list.append(dict(
                    slot_select=f"""{empid},{ol['OLID']},'{slot['Value']}'""",
                    slot_name=slot['Name']
                ))
            day_short = day['Date'].strftime("%A")[:3]
            days_dict[f"{day_short}_{ol['OLID']}".lower()] = dict(
                olid=ol['OLID'],
                day_short=day_short,
                date=f"""{day['Date'].day}""",
                slot_list=slot_list
            )

        first_open_day = None
        first_day = None
        for key, value in days_dict.items():
            if not first_day:
                first_day = (key, value)
            if value["slot_list"] and not first_open_day:
                first_open_day = (key, value)
        if not first_open_day:
            first_open_day = first_day

        tpl = get_template("somos/site_list_calendar.html")
        mc += tpl.render(dict(days_dict=days_dict, fod=first_open_day))

        mc += '</div>'

        mc += '</div></div>'
        mc += '</div>'

    return mc, num_of_sites




