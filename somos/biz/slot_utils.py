import datetime
from PhmWeb.common import DateUtils, DictUtils
from PhmWeb.utils.dbconfig import sql_ec_db
from somos.biz.slot_cache import SlotCache
from somos.biz.slot_daily_setting import get_location_slot_daily_limit


class SlotUtils:

    @staticmethod
    def get_slot_key(location, apt_time):
        schedule_period = SlotUtils.get_schedule_period_by_location(location)
        # apt_time = datetime.datetime.now()
        mm = apt_time.minute
        if mm % schedule_period == 0:
            slot_time = apt_time.replace(second=0)
        else:
            slot_time = apt_time.replace(minute=mm - mm % schedule_period, second=0) + datetime.timedelta(minutes=schedule_period)  # change to start to 0,10,20
        apt_ss = DateUtils.datetime_to_long_string(slot_time, same_timezone=True)
        slot_key = f'{location}_{apt_ss}'
        return slot_key  # Location_yyyyMMdd_HHMM

    @staticmethod
    def get_slot_key_day(location, apt_time):
        apt_day = DateUtils.datetime_to_short_string(apt_time)
        slot_key = f'{location}_{apt_day}'
        return slot_key

    @staticmethod
    def get_schedule_period_by_location(location_id): # 5 or 10
        return SlotCache.instance().get_schedule_period_by_location(location_id)

    @staticmethod
    def get_apt_upper_limit_by_location(location_id): # > 1
        return SlotCache.instance().get_apt_upper_limit_by_location(location_id)

    @staticmethod
    def update_location_interval_to_cache(ol_list):
        return SlotCache.instance().update_location_interval_to_cache(ol_list)

    # 4 - validation for zip code.  only 1397 hardcode for zip v 2/3/2021
    # 0 - valid, 1 code format is invalid, 2 not valid by site

    @staticmethod
    def get_zip_enforced_olids():
        # TODO - cache
        ec_db = sql_ec_db(as_dict=True)

        sql_zip_restricted_sites = f"""
        select ol.OLID from Ref_ClinicOfficeLocation ol where upper(ol.ZipcodeValidation) = 'YES'
        """

        res_list = ec_db.fetch_sql_dict_list(sql_zip_restricted_sites)
        restricted_olids = list(map(lambda e: e["OLID"], res_list))

        ec_db.close_conn()

        return restricted_olids

    @staticmethod
    def get_olid_zip_dict():
        # TODO - cache
        ec_db = sql_ec_db(as_dict=True)

        sql = f"""
        select ol.OLID, zv.zipcode from Ref_ClinicOfficeLocation ol left join zipValidation zv on ol.OLID = zv.locationID
        """

        res_list = ec_db.fetch_sql_dict_list(sql)
        res_dict = {}

        for i in res_list:
            o = i["OLID"]
            z = i["zipcode"]
            l = res_dict.setdefault(o, [])
            if z:
                l.append(z)

        ec_db.close_conn()

        return res_dict


    @staticmethod
    def is_valid_zipcode(location_id, zipcode):

        if zipcode is None or len(zipcode) != 5:
            return 1

        zip_olids = SlotUtils.get_zip_enforced_olids()
        zip_dict = SlotUtils.get_olid_zip_dict()

        if location_id not in zip_olids:
            return 0

        if zipcode in zip_dict[location_id]:
            return 0

        return 2


    @staticmethod
    # 1 - vacation, 2 - exceed total capacity, 3 - slot occupied by others , 0 - OK
    def is_able_to_commit_appointment(location_id, book_time):
        # step 1 # validate  vacation days, if it is weekday
        # book_day = '12/08/2010' # 12/08/2010
        ec_db = sql_ec_db(as_dict=True)
        book_day = DateUtils.datetime_to_short_string(book_time)
        sql_vacation = """select count(1) as Vacation from Employee_Vacation2010 vk ,Ref_ClinicOfficeLocation ol 
                                where vk.EmployeeID=ol.MedicalDirector and ol.OLID = {0} and vk.VacationDate='{1}' """.format(location_id, book_day)
        print(f'sql_vacation={sql_vacation}')

        vacation_list = ec_db.excute_select(sql_vacation)
        if len(vacation_list) > 0 and vacation_list[0]['Vacation'] > 0:
            return 1

        # step 2: validate total appointment have been made in the day
        if not SlotUtils.is_valid_by_daily_appointment_count(ec_db, location_id, book_time):
            return 2

        # stop 3: check whether the slot is still available
        sql_location_param = "select OLScheduleInterval, ol.OLMaxApptCount from Ref_ClinicOfficeLocation ol  where ol.OLID={0}".format(location_id)
        location_param = ec_db.excute_select(sql_location_param)
        location_param = location_param[0] if len(location_param) > 0 else dict({'OLScheduleInterval': 10, 'OLMaxApptCount': 1})

        schedule_interval = location_param['OLScheduleInterval']

        period_begin = book_time.replace(minute=0, second=0)
        period_end = period_begin
        for steps in range(1, 13):
            if period_end > book_time:
                break
            period_begin = period_end
            period_end = period_end + datetime.timedelta(minutes=schedule_interval)

        sql_count_apt = """select count(1) AptCount from Appointment where LocationID={0} and Status not in(3,5, 13)
                            and BeginDateTime >= '{1}' and BeginDateTime< '{2}'""".format(location_id,
                                                                                          DateUtils.date_to_long_string(period_begin, same_timezone=True),
                                                                                          DateUtils.date_to_long_string(period_end, same_timezone=True))
        apt_count = ec_db.excute_select(sql_count_apt)
        apt_count = apt_count[0]['AptCount'] if len(apt_count) > 0 else 0
        print(f"{sql_count_apt}, book_time={book_time}, apt_count{apt_count}, OLMax={location_param['OLMaxApptCount']}")
        if apt_count >= location_param['OLMaxApptCount']:
            return 3
        return 0

    @staticmethod
    def is_valid_by_daily_appointment_count(ec_db, location_id, book_time):
        book_day = DateUtils.datetime_to_short_string(book_time)
        location_day_key = '{0}_{1}'.format(location_id, book_day)
        LOCATIONID_DAY_SLOTS = get_location_slot_daily_limit() # get from redis

        MAX_TOTAL_DAILY_APPOINTMENT = 50 # the default value is 10 which it means nothing and this value must be set to more than 10

        if location_day_key in LOCATIONID_DAY_SLOTS:
            MAX_TOTAL_DAILY_APPOINTMENT = LOCATIONID_DAY_SLOTS[location_day_key]['Slots']
            # if MAX_TOTAL_DAILY_APPOINTMENT > 500:  MAX_TOTAL_DAILY_APPOINTMENT = 500
        print(f'is_valid_by_daily_appointment_count, location_day_key={location_day_key},MAX_TOTAL_DAILY_APPOINTMENT={MAX_TOTAL_DAILY_APPOINTMENT}')
        next_day = book_time + datetime.timedelta(days=1)

        sql_count = """select count(1) AptCount from Appointment where LocationID={0} and Status not in(3, 5, 13)
                            and BeginDateTime >= '{1}' and BeginDateTime< '{2}'""".format(location_id,
                                                                                          book_day,
                                                                                          DateUtils.datetime_to_short_string(next_day))

        toal_apt_count = ec_db.excute_select(sql_count)
        toal_apt_count = toal_apt_count[0]['AptCount'] if len(toal_apt_count) > 0 else 0
        print(f'{sql_count}, {toal_apt_count}')
        is_reach_upper_list = toal_apt_count >= MAX_TOTAL_DAILY_APPOINTMENT
        if is_reach_upper_list: # change it to holiday
            sql_vacation = '''
                declare @emp nvarchar(100), @cli int;
                select @emp =MedicalDirector,@cli=ClinicID   from Ref_ClinicOfficeLocation where OLID={0};
                if not exists(select 1 from Employee_Vacation2010 where EmployeeID=@emp and ClinicID=@cli and VacationDate ='{1}')
                    insert into Employee_Vacation2010 ( EmployeeID,ClinicID,VacationDate) values (@emp,@cli ,  '{1}')
                            '''.format(location_id, book_day)
            # print(f'sql_update_vacation={sql_vacation}')
            ec_db.excute_update(sql_vacation)
        return not is_reach_upper_list
