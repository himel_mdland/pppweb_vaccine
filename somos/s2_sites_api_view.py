import datetime, json, bson
from collections import OrderedDict
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.templatetags.static import static
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.dbmodel.DbPhmLogRestAPI import DbPhmLogRestAPI
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire
from PhmWeb.settings import PHMSettings
from somos.employee_slot import EmployeeSlot, SlotDataService, SlotUtils
from somos.biz.gmap_utils import GmapUtils
from somos.biz.apt_secure_biz import AptSecureBiz
from collections import OrderedDict
from django.template.loader import get_template


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action == 'getSiteWithSlotAPI':
        return get_site_with_slot(request)


def get_site_with_slot(request):
    res_dict = dict()
    search_day = RequestUtils.get_string(request, 'day')
    if search_day is None or search_day == '':
        search_day = DateUtils.datetime_to_short_string(datetime.datetime.now())
    dt_search_day = DateUtils.string_to_datetime(search_day)

    step = RequestUtils.get_long(request, 'step')
    if step != 0:
        dt_search_day = dt_search_day + datetime.timedelta(days=step)
        search_day = DateUtils.datetime_to_short_string(dt_search_day)

    res_dict['BookingDay'] = search_day

    res_dict["Prev"] = True
    if datetime.datetime.now().date() >= dt_search_day.date():
        res_dict["Prev"] = False

    # only search recent 50 days
    dt_delta = (dt_search_day - datetime.datetime.now()).days
    print(f'dt_search_day={dt_search_day}, dt_delta={dt_delta}')
    if dt_delta > 35 or dt_delta < -5:
        res_dict['Count'] = 0
        res_dict['Content'] = ''
        return JsonResponse(res_dict)

    address = str(RequestUtils.get_string(request, 'zipcode').strip() or "11106")
    location_id = RequestUtils.get_long(request, 'locationID') # edit appointment can not change site
    location_name = ''

    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    slot_data_service = SlotDataService(clinic_id=clinic_id)
    emp_list = slot_data_service.get_em_with_location(location_id=location_id)
    slot_lock_dict = slot_data_service.get_in_lock_appoint(dt_search_day, dt_search_day + datetime.timedelta(days=6))  # all locations' appointment
    emp_vacation_dict = slot_data_service.get_employee_vacation(dt_search_day, dt_search_day + datetime.timedelta(days=6)) # all locations' vaction
    overnight_dict = slot_data_service.get_location_overnight_setting()
    # print(f'emp_list={emp_list}')

    vaccine_offered = RequestUtils.get_string(request, 'vaccineOffered')
    # print(f'vaccine_offered={vaccine_offered}')
    if vaccine_offered is not None and len(vaccine_offered) > 5:
        emp_list = [x for x in emp_list if 'OfficeLocation' in x and x.get('OfficeLocation') is not None]

        emp_list = [x for x in emp_list if
                       (vaccine_offered.find('Johnson18') >= 0 and x['OfficeLocation'].get('Johnson18') is not None and x['OfficeLocation'].get('Johnson18') == 1)
                    or (vaccine_offered.find('Moderna18') >= 0 and x['OfficeLocation'].get('Moderna18') is not None and x['OfficeLocation'].get('Moderna18') == 1)
                    or (vaccine_offered.find('Pfizer12') >= 0 and x['OfficeLocation'].get('Pfizer12') is not None and x['OfficeLocation'].get('Pfizer12') == 1)
                    or (vaccine_offered.find('Pfizer0511') >= 0 and x['OfficeLocation'].get('Pfizer0511') is not None and x['OfficeLocation'].get('Pfizer0511') == 1)
                    ]

    distance_res = slot_data_service.fill_em_with_distance(emp_list, address)
    if distance_res['ResultCode'] > 0:
        return JsonResponse(distance_res)

    emp_gmap_list = distance_res['EmpList']

    json = RequestUtils.get_long(request, 'json')
    if json == 1:
        res_dict['emp_gmap_list'] = emp_gmap_list
        res_dict['slot_lock_dict'] = slot_lock_dict
        res_dict['emp_vacation_dict'] = emp_vacation_dict
        res_dict['LocationName'] = location_name
        res_dict['GAddr'] = distance_res['Result']["origin_addresses"][0] if 'origin_addresses' in distance_res['Result'] else address
        return res_dict

    mc, num_of_sites, location_name = render_to_phmweb(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, location_id) # bd.mdland.com , API

    res_dict['Content'] = mc
    res_dict['Count'] = num_of_sites
    res_dict['LocationName'] = location_name
    res_dict['GAddr'] = distance_res['Result']["origin_addresses"][0] if 'origin_addresses' in distance_res['Result'] else address
    return JsonResponse(res_dict)


# for https://sv.mdland.com  3553;  https://rendr360.mdland.com 3552, called by API
def render_to_phmweb(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, location_id):
    mc = '<table class="table">'
    num_of_sites = 0
    location_name = ''
    for idx, emp in enumerate(emp_gmap_list):
        empid = emp['EmployeeID']
        ol = emp['OfficeLocation']

        emp_slot = EmployeeSlot(period_days=5)
        emp_slot.set_week_schedule(emp)  #
        emp_slot.set_inlock_appoint_dict(slot_lock_dict)
        emp_slot.set_employee_vacation(emp_vacation_dict)
        slot_interval = SlotUtils.get_schedule_period_by_location(location_id=ol['OLID'])
        day_list = emp_slot.get_slot_by_period(location_id=ol['OLID'], day_begin=search_day, slot_interval=slot_interval)
        if len(day_list) == 0: # all slot of the location are empty
            continue
        num_of_sites += 1
        if PHMSettings.DISABLE_GMAPS_CACHE:
            map = GmapUtils.gmap_img(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        else:
            map = GmapUtils.gmap_img_from_cache(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        mc += '<tr olid={0}>'.format(ol.get('OLID'))

        if location_id <= 0:
            mc += f'''<td style="width:400px;height:245px;background: url('https://pp.mdland.com%s' ) center no-repeat, url('https://pp.mdland.com%s') center no-repeat;">''' % (static('images/somos-emblem-marker.png'), map)
            senior_only = '<div class="text-danger">Currently, this site is seeing seniors <b>65+ only</b>.</div>' if ol['SeniorOnly'] else ''
            temp_only = '<div class="text-danger">All vaccine appointments between 9:00am and 12:00pm from 2/12 to 2/21 are ONLY for DOE staff from D75, elementary, and middle schools who are scheduled to work in-person.</div>' if str(ol) in ("1367", "1368", "1369") else ""
            print("temp:%s" % temp_only)
            mc += f'''<div style='background-color:white;height:100px'>
                    <h5>{idx+1}. {ol['Name']}</h5>
                    <div>{ol['Phone']}</div>
                    <div><b>{ol['Address']}, {ol['City']}, {ol['State']} - {ol['Zip']}</b></div>
                    <b>{emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''}  away</b>
                     {senior_only}{temp_only}
                    </div>
                  </td>
            '''
        else:
            location_name = ol['Name']

        for day in day_list:
            mc += '''<td style="background-color: white; margin-left:5px; margin-right: 5px; width: 14%; text-align: center">'''
            mc += f'''<h5 style="margin-top:18px; font-weight:bold">{day['Date'].strftime("%A")[:3]}<br><span style="white-space: nowrap;">{day['Date'].strftime("%b")} {day['Date'].day}</span></h5>'''
            mc += '''<div class="row" style="overflow-y: scroll; height: 245px; margin-bottom: 18px;">'''

            if len(day['SlotList']) == 0:
                mc += "<button class='appointment-slot-closed btn disabled btn-sm ' style='margin:5px; cursor: not-allowed;'>NA</button>"
            for slot in day['SlotList']:
                mc += f'''<button class="appointment-slot btn btn-sm btn-primary" style="margin:5px; background-color:#65b392;" onclick="selectSlot({empid},{ol['OLID']},'{slot['Value']}')" >{slot['Name']}</button>'''
            mc += '</div></td>'
        mc += '</tr>'
    mc += '</table>'
    return mc, num_of_sites, location_name


