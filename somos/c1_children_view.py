import datetime, json, bson
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action == 'saveQuestionnaire':
        return save_questionnaire(request)
    else:
        return init_html_page(request)


def init_html_page(request):
    page_dict = dict()
    smg = SessionManager(request)
    page_dict['Lang'] = smg.get_lang_dict()

    i18n = request.GET.get('i18n', None)
    page_dict['i18n'] = i18n
    return render(request, 'somos/child5/covid_children_under_5.html', page_dict)


def save_questionnaire(request):
    res_dict = dict()
    biz = IcCovidRegQuestionnaire()
    qid = biz.save_intake_form(request)
    res_dict['qid'] = qid
    res_dict['ResultCode'] = 0 if qid != '' else 1
    if qid == '':
        res_dict['Error'] = biz.get_error_info()

    return JsonResponse(res_dict)
