import datetime, threading
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.common import DateUtils, DictUtils
from somos.biz.gmap_utils import GmapUtils
from somos.biz.slot_utils import SlotUtils
from PhmWeb.biz.redis_cache import RedisCache


class EmployeeSlot:
    def __init__(self, period_days):
        self._employee_id = 0
        self._week_schedule = dict()
        self._emp_vacation_dict = None  # all locations' vacation
        self._inlock_appoint_dict = list()  # all locations' appoint
        self._period_days = period_days
        self._overnight_dict = None # somos_apt_OverNightSite 3/10/2021

    def set_week_schedule(self, val):
        self._week_schedule = val
        # print(f"set_week_schedule={val}")

    def set_inlock_appoint_dict(self, val):
        self._inlock_appoint_dict = val

    def set_employee_vacation(self, val):
        self._emp_vacation_dict = val

    def set_overnight_site(self, val):
        self._overnight_dict = val

    @property
    def EmployeeID(self):
        return self._employee_id

    def get_slot_by_period(self, location_id, day_begin, slot_interval):
        dt_begin = day_begin
        if isinstance(day_begin, str):
            dt_begin = DateUtils.string_to_datetime(day_begin)

        res_list = list()
        is_all_empty = True
        for i in range(self._period_days):
            dt_day = dt_begin + datetime.timedelta(days=i)
            day_slot = self.get_slot_by_day(location_id, dt_day, slot_interval)
            res_list.append(day_slot)
            if len(day_slot['SlotList'])>0:
                is_all_empty = False
        if is_all_empty:
            res_list = []
        return res_list

    def get_slot_by_day(self, location_id, dt_day, slot_interval):
        res = dict({'SlotList': list()})
        # dt_day = datetime.datetime.now()
        res['Date'] = dt_day
        res['StrDay'] = dt_day.strftime('%m/%d')
        # 6/18/2020 vacation
        vacation_key = str(location_id) + '_' + DateUtils.datetime_to_short_string(dt_day)
        if vacation_key in self._emp_vacation_dict:
            return res

        # print(f'day={dt_day}, weekday={dt_day.isoweekday()}') # Sunday is 7.
        week_day_name = ['', 'Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat', 'Sun']
        res['WeekDay'] = week_day_name[dt_day.isoweekday()]

        hour_begin, hour_end, lunch_begin, lunch_end = self.get_working_hours(dt_day)
        slot_list = list()
        dt_begin = dt_day + datetime.timedelta(minutes=60*hour_begin)
        dt_end = dt_day + datetime.timedelta(minutes=60*hour_end)

        if hour_end > hour_begin:
            dt_slot = dt_begin
            while dt_slot < dt_end:
                if dt_slot < datetime.datetime.now():
                    dt_slot += datetime.timedelta(minutes=slot_interval)
                    continue
                if lunch_end > lunch_begin:
                    dt_slot_as_5minutes = dt_slot.hour + dt_slot.minute/ 60.0
                    if lunch_begin <= dt_slot_as_5minutes < lunch_end:
                        dt_slot += datetime.timedelta(minutes=slot_interval)
                        continue

                # 3/10/2021 Overnight 1412
                if location_id == 1412 and vacation_key in self._overnight_dict:
                    ng_setting = self._overnight_dict[vacation_key]
                    is_EarlyMorningSlotFull = DictUtils.get_int_value(ng_setting, 'EarlyMorningSlotFull')
                    is_EveningSlotFull = DictUtils.get_int_value(ng_setting, 'EveningSlotFull')
                    if is_EveningSlotFull or is_EarlyMorningSlotFull:
                        slot_hour = dt_slot.hour
                        if is_EveningSlotFull and slot_hour >= 20:
                            dt_slot += datetime.timedelta(minutes=slot_interval)
                            continue
                        if is_EarlyMorningSlotFull and slot_hour <=8:
                            dt_slot += datetime.timedelta(minutes=slot_interval)
                            continue

                # print(f'get_slot_by_day.location_id={location_id}, slot_interval={slot_interval}')
                slot_key = SlotUtils.get_slot_key(location_id, dt_slot)
                slot_item = dict({"Name": dt_slot.strftime('%I:%M %p'), 'Value': dt_slot.strftime('%m/%d/%Y %H:%M:%S'), 'IsMine':0})
                if dt_slot.hour == 0:
                    slot_item["Name"] = '00' + slot_item["Name"][2:]
                if slot_key not in self._inlock_appoint_dict: # if it is locked # 5/29/2020
                    slot_list.append(slot_item)
                else:
                    if not self._inlock_appoint_dict[slot_key]['IsFullOccupied']:  # 6/13/202 double booking
                        slot_list.append(slot_item)
                dt_slot += datetime.timedelta(minutes=slot_interval)

        res['SlotList'] = slot_list
        return res

    def get_working_hours(self, working_day):
        hour_begin = 0
        hour_end = 0
        delta = working_day - datetime.datetime.now()
        # same day , delta.days == -1:
        max_begin_hour = 0
        if delta.days < -1:
            return hour_begin, hour_end, 0, 0
        elif delta.days == -1:
            # 4/20/2021 by Eugene change from +2 to +0
            max_begin_hour = datetime.datetime.now().hour + 0
        my_ios_week_day = working_day.isoweekday() % 7  # 0-Sunday, 1-Monday
        begin_hour_key = 'BeginHour{0}'.format(my_ios_week_day)
        end_hour_key = 'EndHour{0}'.format(my_ios_week_day)

        hour_begin = (DictUtils.get_int_value(self._week_schedule, begin_hour_key) * 5 / 60)
        hour_end = (DictUtils.get_int_value(self._week_schedule, end_hour_key) * 5 / 60)

        if max_begin_hour > hour_begin:
            hour_begin = max_begin_hour
        if max_begin_hour > hour_end:
            hour_begin = 0
            hour_end = 0

        # 3/4/2021 LunchTimes # 144-156=0|144-156=1|144-156=2|144-156=3|144-156=4|144-156=5|144-156=6  12:00 - 13:00
        lunch_begin = 0
        lunch_end = 0
        lunch_times = DictUtils.get_value(self._week_schedule, 'LunchTimes')
        if lunch_times and lunch_times != '0-0=0|0-0=1|0-0=2|0-0=3|0-0=4|0-0=5|0-0=6':
            lunch_time_list = lunch_times.split('|')
            if my_ios_week_day <= len(lunch_time_list)-1:
                lunch_ss = lunch_time_list[my_ios_week_day]
                if not lunch_ss.startswith('0-0'):
                    pos_minus = lunch_ss.find('-')
                    lunch_begin = int(lunch_ss[0: pos_minus]) * 5 / 60
                    lunch_end = int(lunch_ss[pos_minus+1: -2]) * 5 / 60
        return hour_begin, hour_end, lunch_begin, lunch_end


class SlotDataService:
    def __init__(self, clinic_id):
        self._clinic_id = clinic_id
        self._ec_db = sql_ec_db(as_dict=True)

    # return employee with location
    def get_em_with_location(self, location_id):
        ol_list = self.get_em_office_location(location_id)

        location_medical_director = 0
        if location_id > 0:
            location_medical_director = ol_list[0]['EmployeeID']

        emp_list = self.get_em_working_hours(location_medical_director)

        # 5/30/2020 update schedule interval to cache
        SlotUtils.update_location_interval_to_cache(ol_list)

        ol_dict = dict()
        for ol in ol_list:
            ol_dict[ol['EmployeeID']] = ol

        em_ol_list = list()
        for emp in emp_list:
            empid = emp['EmployeeID']
            if empid not in ol_dict:
                continue
            emp['OfficeLocation'] = ol_dict[empid]
            em_ol_list.append(emp)
        # print(f'get_em_with_location.em_ol_list={em_ol_list}')
        return em_ol_list

    # fill distance info
    def fill_em_with_distance(self, emp_list, address):
        dl = []
        for emp in emp_list:
            ol = emp['OfficeLocation']
            emp["destination_id"] = GmapUtils.get_url_safe_full_addr(ol.get("Address", "N/A"),
                                                           ol.get('City', "N/A"),
                                                           ol.get('State', "N/A"),
                                                           ol.get("Zip", "N/A"))
            dl.append(emp["destination_id"])

        print("###########################")
        print("###########################")
        try:
            # result = 1 / 0
            if len(dl) > 0:
                result = GmapUtils.distance_matrix_from_cache(address, dl, "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
            else:
                print('Office Location is NA')
                result = dict({'rows': [{'elements': []}]})
        except:
            print('google map unavailable')
            result = dict({'rows': [{'elements': []}]})
        print("###########################")
        print("###########################")

        result_zip = zip(dl, result["rows"][0]['elements'])

        for pair in result_zip:
            for emp in emp_list:
                if pair[0] == emp["destination_id"]:
                    emp["distance_to_origin"] = pair[1]

        op_dict = dict({'ResultCode': 0, 'EmpList': emp_list}) # when there is no google map support, just return the same list
        try:
            if len(result["rows"][0]['elements']) > 0:
                emp_list = sorted(emp_list, key=lambda e: e["distance_to_origin"]["distance"]["value"])
                op_dict['EmpList'] = emp_list
        except KeyError:
            op_dict['ResultCode'] = 1
            op_dict['Content'] = "<h4><i class='fa fa-fw fa-warning'></i>No Results Found</h4>"
            op_dict['Count'] = 0
            op_dict['GAddr'] = result["origin_addresses"][0] if 'origin_addresses' in result else ''
            op_dict.pop('EmpList')
            return op_dict
        op_dict['Result'] = result
        return op_dict


    def get_em_working_hours(self, medical_director):
        sql_sel = f'''
                select b.EmployeeID,b.EmployeeFirstName,b.EmployeeMidName,b.EmployeeLastName,b.EmployeeType,
                            ew.BeginHour0,ew.BeginHour1,ew.BeginHour2,ew.BeginHour3,ew.BeginHour4
                            ,ew.BeginHour5,ew.BeginHour6,ew.EndHour0,ew.EndHour1,ew.EndHour2,ew.EndHour3,ew.EndHour4,ew.EndHour5,ew.EndHour6 ,isnull(tab.OLNames,'')OLNames,isnull(tab2.LunchTimes,'')LunchTimes
                            from Employee b  
                            left join Ref_Appointment_Employee a on a.ClinicID = b.ClinicID and a.DoctorID = b.EmployeeID
                            left join Employee_WorkingHour ew on ew.EmployeeID= b.EmployeeID and  ew.ClinicID=b.ClinicID
                            left join (	select EmployeeID,ClinicID,OLNames=stuff(
        		                        (select '|'+rtrim(ref.Name)+'='+cast(t.[WeekDay] as varchar) from Employee_WorkingLocation t join Ref_ClinicOfficeLocation ref on ref.OLID=t.OLID 
        		                        and ref.ClinicID=t.ClinicID and ref.Status=1 where t.EmployeeID=tb.EmployeeID and t.ClinicID=tb.ClinicID  
        		                        for xml path('')), 1, 1, '')   from Employee_WorkingLocation tb  group by EmployeeID,ClinicID
                                     ) tab on tab.EmployeeID=b.EmployeeID and tab.ClinicID=b.ClinicID
                            left join(select EmployeeID,ClinicID,LunchTimes=stuff(
        		                        (select '|'+cast(t.BeingHour as varchar)+'-'+cast(t.EndHour as varchar)+'='+cast(t.[WeekDay] as varchar) from Employee_LunchTime t  where t.EmployeeID=tb.EmployeeID and t.ClinicID=tb.ClinicID  
        		                        for xml path('')), 1, 1, '')   from Employee_LunchTime tb  group by EmployeeID,ClinicID
                                    ) tab2 on tab2.EmployeeID=b.EmployeeID and tab2.ClinicID=b.ClinicID
                            Where b.EmployeeStatus=0 And b.ClinicID = {self._clinic_id} '''
        if medical_director > 0:
            sql_sel += f' and b.EmployeeID= {medical_director} '
        else:
            sql_sel += ' and a.DoctorID = b.EmployeeID '
        sql_order = " order by a.DisplayOrder"
        sql_working_hour = sql_sel + sql_order
        # print(sql_working_hour)
        emp_list = self._ec_db.excute_select(sql_working_hour)
        return emp_list

    def get_em_office_location(self, location_id):
        # emp_ids = ','.join([str(x['EmployeeID']) for x in emp_list]) if len(emp_list) > 0 else '0'

        #sql_ol = f'''select ol.*, my.EmployeeID  from Ref_ClinicOfficeLocation ol inner join Ref_MyOfficeLocation my on ol.OLID=my.OLID
        #             where ol.Status=1 and my.EmployeeID in ({emp_ids}) and ol.ClinicID={self._clinic_id}'''

        sql_ol = f'''select ol.OLID, ol.ClinicID,ol.Name, ol.Address, ol.City, ol.State, ol.Zip, ol.Phone,  ol.Status, ol.WorkBeginHour, ol.WorkEndHour, 
                        ol.OLScheduleInterval, ol.OLMaxApptCount, ol.OLMaxApptToday, ol.SeniorOnly, ol.ZipcodeValidation, ol.SiteSubTitle, ol.CustomerSiteDescription,
                        ol.MedicalDirector as EmployeeID ,ol.Johnson18, ol.Moderna18,ol.Pfizer12, ol.Pfizer0511, ol.EligibleUnderAge5, ol.EligibleUnderAge18, ol.Pfizer05 , ol.Moderna05
                         from Ref_ClinicOfficeLocation ol 
                    where ol.Status=1 and ol.MedicalDirector >0 and ol.ClinicID={self._clinic_id}'''
        if location_id > 0:
            sql_ol += f' and ol.OLID={location_id}'
        ol_list = self._ec_db.excute_select(sql_ol)
        return ol_list

    def get_in_lock_appoint(self, begin_date, end_date):
        day_begin = DateUtils.datetime_to_short_string(begin_date) if isinstance(begin_date, datetime.datetime) else begin_date
        day_end = DateUtils.datetime_to_short_string(end_date) if isinstance(end_date, datetime.datetime) else end_date

        # add to redis cache
        # dt_now_minute = datetime.datetime.now().strftime("%m%d%H%M")
        dt_now_minute = datetime.datetime.now().strftime("%m%d%H")
        mm = datetime.datetime.now().minute // 5
        dt_now_minute += f'_m5_{mm}'

        dt_key = "apt_lot_{0}_{1}_{2}".format(day_begin, day_end, dt_now_minute)
        cache_dict = RedisCache.instance().get_lock_apt_info(dt_key)
        if cache_dict is not None:
            return cache_dict

        sql = f'''
            select LocationID, BeginDateTime, '' as LockID , COUNT(1) as LockNum from
              (select apt.LocationID, apt.BeginDateTime from Appointment apt where apt.ClinicID={self._clinic_id} and apt.Status !=3
                  and apt.BeginDateTime  between '{day_begin}' and '{day_end}' 
				  and not exists 
				  ( select ol.OLID, vk.* from Employee_Vacation2010 vk, Ref_ClinicOfficeLocation ol where vk.EmployeeID=ol.MedicalDirector
				  and ol.OLID=apt.LocationID and DATEDIFF(Day, apt.BeginDateTime, vk.VacationDate) =0
				 ) )vapt group by LocationID, BeginDateTime 	
              union all
            select BookingLocationID, BookingPeriod, convert(nvarchar(50),Qid) as LockID , 1 as LockNum from somos_CovidRegQuestionnaire where BookingLockDate >DATEADD(MINUTE, -11,GETDATE())
               and BookingPeriod between '{day_begin}' and '{day_end}' and AppointmentID is null
            order by LocationID, BeginDateTime
        '''
        # print(f'get_in_lock_appoint={sql}')
        apt_list = self._ec_db.excute_select(sql)
        lock_dict = dict() # key="Location_yyyyMMdd_HHMM"
        for apt in apt_list:
            olid = apt['LocationID']
            apt_time = apt['BeginDateTime']
            dict_key = SlotUtils.get_slot_key(olid, apt_time)
            lock_num = apt['LockNum'] # number of appointments in the location and time
            if dict_key not in lock_dict:
                lock_dict[dict_key] = {'SlotAptUpperLimit': SlotUtils.get_apt_upper_limit_by_location(olid), 'InSlotAptCount': lock_num, 'LockerId': []}
            else:
                lock_dict[dict_key]['InSlotAptCount'] += lock_num
            if apt['LockID']:
                lock_dict[dict_key]['LockerId'].append(apt['LockID'])
            lock_dict[dict_key]['IsFullOccupied'] = lock_dict[dict_key]['InSlotAptCount'] >= lock_dict[dict_key]['SlotAptUpperLimit']

        # save to cache
        RedisCache.instance().cache_lock_apt_info(lock_key=dt_key, apt_dict=lock_dict)
        return lock_dict

    # if lock exits or not
    def is_available_to_book(self, location, book_time, my_locker=None):
        begin_hour = book_time - datetime.timedelta(hours=1)
        end_hour = book_time + datetime.timedelta(hours=1)
        lock_dict = self.get_in_lock_appoint(DateUtils.date_to_long_string(begin_hour, same_timezone=True),
                                             DateUtils.date_to_long_string(end_hour, same_timezone=True))
        my_slot_key = SlotUtils.get_slot_key(location, book_time)
        if my_slot_key in lock_dict:
            slot_locker = lock_dict[my_slot_key]
            if my_locker is not None and my_locker in slot_locker['LockerId']: # my current locker
                return True

            # double booking
            if not slot_locker['IsFullOccupied']:
                return True

            return False  # not available
        return True

    # return dict{'OLID_MM/dd/yyyy':{EmployeeID,VacationDate, OLID}}
    def get_employee_vacation(self, begin_date, end_date):
        day_begin = DateUtils.datetime_to_short_string(begin_date) if isinstance(begin_date, datetime.datetime) else begin_date
        day_end = DateUtils.datetime_to_short_string(end_date) if isinstance(end_date, datetime.datetime) else end_date
        sql = f"""select va.EmployeeID, FORMAT(va.VacationDate, 'MM/dd/yyyy') VacationDate , ol.OLID
                    from Employee_Vacation2010 va inner join Ref_ClinicOfficeLocation ol on va.EmployeeID=ol.MedicalDirector
                         where va.ClinicID='{self._clinic_id}' and va.VacationDate between '{day_begin}' and '{day_end}' """
        # print(sql)
        voc_list = self._ec_db.excute_select(sql)
        emp_day_dict = dict()
        for item in voc_list:
            key = '{0}_{1}'.format(item['OLID'], item['VacationDate'])
            emp_day_dict[key] = item
        return emp_day_dict

    def get_location_overnight_setting(self):
        sql = ''' select ns.LocationID, ns.VacationDate,ns.EarlyMorningSlotFull, ns.EveningSlotFull
                    from somos_apt_OverNightSite ns where  ns.ClinicID={0} and ns.VacationDate > DATEADD(DAY, -2, GETDATE())
                    '''.format(self._clinic_id)
        ns_list = self._ec_db.excute_select(sql)
        night_dict = dict()
        for ns in ns_list:
            location_id = ns['LocationID']
            day = DictUtils.get_short_string_value(ns, 'VacationDate')
            dict_key = f'{location_id}_{day}'
            night_dict[dict_key] = ns
        return night_dict
