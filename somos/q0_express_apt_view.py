import datetime, json, bson, uuid
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.templatetags.static import static
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire
from PhmWeb.settings import PHMSettings
from collections import OrderedDict


def index(request):
    location_id = RequestUtils.get_long(request, 'sid')
    qid = create_express_questionnaire(request, location_id)
    url = '/p/sites/?locationID={0}&qid={1}'.format(location_id, qid)
    print(url)
    return redirect(url)


def create_express_questionnaire(request, location_id):
    current_ip = RequestUtils.get_request_remote_addr(request)
    qid = uuid.uuid1()
    sql_insert = """  insert into somos_CovidRegQuestionnaire (QID, TestingFor, CreatedDate, BookingLocationID, SiteMode, BookingRequestIP) 
                    values ('{0}', 'Vaccine', getdate(), {1}, 'Express', '{2}')""".format(qid, location_id, current_ip)
    print(sql_insert)
    sql_ec_db().excute_update(sql_insert)
    return qid