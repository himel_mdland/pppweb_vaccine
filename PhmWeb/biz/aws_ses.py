# simple email service
import boto3, datetime
from botocore.exceptions import ClientError
from PhmWeb.utils.dbconfig import get_PHM_db

# Replace sender@example.com with your "From" address.
# This address must be verified with Amazon SES.
#SENDER = "PopHealth<pophealth@mdland.biz>"
#SENDER = "PopHealth<info@empirehealthmso.com>"
SENDER = "PopHealth<do_not_reply@mdland.biz>"

# Replace recipient@example.com with a "To" address. If your account
# is still in the sandbox, this address must be verified.
#RECIPIENT = "rhu@mdland.net"
RECIPIENT = "aaa168@126.com"

# Specify a configuration set. If you do not want to use a configuration
# set, comment the following variable, and the
# ConfigurationSetName=CONFIGURATION_SET argument below.
CONFIGURATION_SET = "ConfigSet"

# If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
AWS_REGION = "us-east-1"

# The character encoding for the email.
CHARSET = "UTF-8"

# Create a new SES resource and specify a region.
client = boto3.client('ses', aws_access_key_id='AKIA4XVQLR4SWPXBPLPA', aws_secret_access_key='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',
                      region_name=AWS_REGION)

# C:\Users\USERNAME\.aws\credentials

class AwsSES:

    def __init__(self):
        self._zw = get_PHM_db()

    def send_email(self, reciptent, subject, content, log_param=None):
        res = dict({'Code': -1})
        # Try to send the email.
        mylog = log_param if log_param is not None else dict()
        try:
            # Provide the contents of the email.
            response = client.send_email(
                Destination={'ToAddresses': [reciptent]},
                Message={
                    'Body': {'Html': {'Charset': CHARSET, 'Data': content}},
                    'Subject': {'Charset': CHARSET, 'Data': subject}
                },
                Source=SENDER,
                # If you are not using a configuration set, comment or delete the
                # following line
                # ConfigurationSetName=CONFIGURATION_SET,
            )
        # Display an error if something goes wrong.
            res['Code'] = 1
            print("Email successfully sent to {1}! return res={0}".format(res, reciptent)),
            res['MessageId'] = (response['MessageId'])
            mylog['ResultCode'] = 1
            mylog['MessageId'] = res['MessageId']
        except ClientError as e:
            res['Error'] = e.response['Error']['Message']
            mylog['ResultCode'] = -1
            mylog['Error'] = res['Error']
            print(res)

        print(mylog)
        self.log_email(reciptent, subject, content, mylog)
        return res

    def log_email(self, reciptent, subject, content, log_param=None):
        log = dict()
        log['Subject'] = subject
        log['Content'] = content
        log['Reciptent'] = reciptent
        log['CreatedDate'] = datetime.datetime.now()
        log['SystemCode'] = 'pppvac'
        if log_param is not None:
            for key in log_param:
                log[key] = log_param[key]
        self._zw['phm_log_AwsEmail'].insert(log)

# C:\Users\USERNAME\.aws\credentials
# ls  ~/.aws
if __name__ == '__main__':
    biz = AwsSES()
    biz.send_email('aaa168@126.com', 'Hello World', 'Hello')

