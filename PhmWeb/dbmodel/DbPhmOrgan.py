import threading, datetime
from PhmWeb.utils.dbconfig import get_PHM_db, get_DW_db


class DbPhmOrgan:
    _instance_lock = threading.Lock()

    def __init__(self):
        self._organ_measure_dict = dict()
        zw = get_PHM_db()
        self._measure_list_all = list(zw['phm_Measure'].find({'CareGapRelease': 1}).sort([('MeasureCategory', 1), ('MeasureID', 1)]))
        self._organ_dict = dict()

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmOrgan, "_instance"):
            with DbPhmOrgan._instance_lock:  # 为了保证线程安全在内部加锁
                if not hasattr(DbPhmOrgan, "_instance"):
                    DbPhmOrgan._instance = DbPhmOrgan(*args, **kwargs)
        return DbPhmOrgan._instance

    def get_organ_by_id(self, organ_id):
        if organ_id not in self._organ_dict:
            zw = get_PHM_db()
            organ = zw['phm_Organ'].find_one({'OrganId': organ_id})
            self._organ_dict[organ_id] = organ
            return organ
        return self._organ_dict[organ_id]

    def reset_cache(self):
        self._organ_dict.clear()

    def get_released_measure_list(self, organ_id):
        res_list = []
        if organ_id in self._organ_measure_dict:
            return self._organ_measure_dict[organ_id]
        if organ_id < 1000:
            zw = get_PHM_db()
            org = zw['phm_Organ'].find_one({'OrganId': organ_id}, {'MeasureList': 1})
            if org is not None and 'MeasureList' in org:
                measure_id_list = org['MeasureList']
                for mr_id in measure_id_list:
                    for dbmr in self._measure_list_all:
                        if dbmr['MeasureID'] == mr_id:
                            res_list.append(dbmr)
            else:
                res_list = self._measure_list_all
            self._organ_measure_dict[organ_id] = res_list

        if len(res_list) == 0:
            res_list = self._measure_list_all

        return res_list

    def get_released_measure_ids(self, organ_id):
        mr_list = self.get_released_measure_list(organ_id)
        id_list =[x['MeasureID'] for x in mr_list]
        return id_list

    def get_member_clinic(self, organ_id):
        organ = self.get_organ_by_id(organ_id)
        if organ is None or 'MemberClinic' not in organ:
            return []
        return organ['MemberClinic']

    def find_primary_care_clinic_id(self, organ_id):
        mb_list = self.get_member_clinic(organ_id)
        if mb_list is None:
            return []
        return [x['ClinicID'] for x in mb_list if 'PrimaryCareClinic' not in x or x['PrimaryCareClinic'] == 2]

    def save_appointment_link_employee(self, organ_id, clinic_id, emp_id, operator):
        zw = get_PHM_db()
        organ = zw['phm_Organ'].find_one({'OrganId': organ_id})
        if organ is None or 'MemberClinic' not in organ:
            return False, 'OrganID={0} Not exists'.format(organ_id)
        member_clinic_list = organ['MemberClinic']
        is_success = False
        err_msg = ''
        for cli in member_clinic_list:
            if cli['ClinicID'] == clinic_id:
                dw = get_DW_db()
                emp = dw['Provider'].find_one({'ClinicID': clinic_id, 'EmployeeID': emp_id},{"ClinicID":1, 'EmployeeID':1,'FullName':1,'Title': 1})
                if emp is not None:
                    emp['OperateDate'] = datetime.datetime.now()
                    emp['Operator'] = operator
                    cli['AppointmentOperator'] = emp
                    is_success = True
                    zw['phm_Organ'].update_one({'OrganId':organ_id}, {'$set': {'MemberClinic': member_clinic_list}})
                    # clear cache
                    self.reset_cache()
                else:
                    err_msg = 'Employee Not exists'
                break

        return is_success, err_msg
