import datetime, uuid
from PhmWeb.common import RequestUtils, DateUtils
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.utils.logger import Logger


class IcSubscribe:
    def __init__(self):
        self._ec_db = sql_ec_db(as_dict=True)
        self._col_list = list()
        self._val_list = list()

    def insert_data(self, clinic_id, request):
        self._col_list.clear()
        self._val_list.clear()
        self.add_param('ClinicID', str(clinic_id))
        self.add_request_param(request, 'FirstName', 'PatientFirstName')
        self.add_request_param(request, 'LastName', 'PatientLastName')

        # self.add_request_param(request, 'DOB', 'PatientDOB', 'Date')
        patient_dob = RequestUtils.get_date(request, 'PatientDOB')
        if patient_dob is None or patient_dob.year < 1910:
            return -1
        else:
            patient_dob = DateUtils.datetime_to_short_string(patient_dob)
            self.add_param('DOB', patient_dob)

        self.add_request_param(request, 'Zipcode', 'PatientAddrZip')
        self.add_request_param(request, 'MobilePhone', 'PatientMobilePhone')
        self.add_request_param(request, 'Email', 'PatientEmail')

        cols = ', '.join(self._col_list)
        vals = "','".join(self._val_list)
        vals = "'{0}'".format(vals)
        sql = f"insert into somos_pat_Subscribe({cols}) values ({vals});SELECT scope_identity() as scope_identity"
        sql = sql.replace("'NULL'", 'NULL')

        ret = self._ec_db.excute_update(sql, fetchall=True)
        self._ec_db.close_conn()
        pid = ret[0]['scope_identity'] if len(ret) > 0 else 0
        return pid

    def add_request_param(self, request, col, key, data_type='String'):
        self._col_list.append(col)
        val = RequestUtils.get_safe_sql_string(request, key)

        if data_type == 'Date':
            if val == '' or val is None:
                val = 'NULL'
            elif val.find('.') > 0:
                dt = datetime.datetime.strptime(val, "%m/%d/%Y")
                val = DateUtils.datetime_to_short_string(dt)
            elif len(val) == 8:
                dt = DateUtils.string_to_datetime(val)
                val = DateUtils.datetime_to_short_string(dt)

        self._val_list.append(val.replace("'", "''"))

    def add_param(self, col, val):
        self._col_list.append(col)
        self._val_list.append(val)
