import datetime, uuid
from PhmWeb.common import RequestUtils, DateUtils
from PhmWeb.utils.dbconfig import sql_ec_db


class IcCovidRegQuestionnaire:
    def __init__(self):
        self._col_list = list()
        self._val_list = list()
        self._error_list = list()

    def get_error_info(self):
        return ','.join(self._error_list)

    def save_intake_form(self, request):
        self._col_list.clear()
        self._val_list.clear()
        self._error_list.clear()

        qid = f'{ uuid.uuid1()}'
        self.add_param('QID',qid)
        testing_for = ''
        if RequestUtils.get_string(request, 'covidTestQualified') == 'true':
            testing_for = 'COVID'
        elif RequestUtils.get_string(request, 'immunityTestQualified') == 'true':
            testing_for = 'Immunity'
        else:
            testing_for = RequestUtils.get_string(request, 'TestingFor')
        self.add_param('TestingFor', testing_for)
        # self.add_request_param(request, 'TestingFor', 'testing_for')
        self.add_request_param(request, 'SymptomFirstDate', 'datepicker_date_first_symptoms', 'Date')
        self.add_request_param(request, 'SymptomLasting', 'lengthOfSymptoms')
        self.add_request_param(request, 'SymptomLastDay', 'datepicker_date_last_symptoms', 'Date')
        self.add_request_param(request, 'RespiratorySymptom', 'lastSymptoms')
        self.add_request_param(request, 'LossTasteOrSmell', 'lossOfSenses')
        self.add_request_param(request, 'Fever', 'fever')
        self.add_request_param(request, 'CloseContactCovid', 'contactWith')
        self.add_request_param(request, 'LiveWith', 'liveWith') #
        self.add_request_param(request, 'CovidTested', 'testedBefore')
        self.add_request_param(request, 'CovidTestDate', 'datepicker_covide_test_date', 'Date')
        self.add_request_param(request, 'CovidTestResult', 'datepicker_covide_test_date_result')
        self.add_request_param(request, 'ChestExam', 'xrayScan')
        # self.add_request_param(request, '', 'iAgree') always on
        self.add_request_param(request, 'Initial', 'iAgreeInitials')
        self.add_request_param(request, 'EmployerRequested', 'employerRequested')
        self.add_request_param(request, 'SiteMode', 'SiteMode')
        self.add_request_param(request, 'Traveling', 'traveling')
        self.add_request_param(request, 'TravelDate', 'datepicker_travel_date', 'Date')

        self.add_request_param(request, 'Q00', 'q0a')
        self.add_request_param(request, 'Q01', 'q0b')
        self.add_request_param(request, 'Q02', 'q2')
        self.add_request_param(request, 'Q03', 'q3')
        self.add_request_param(request, 'Q04', 'q4')
        self.add_request_param(request, 'Q05', 'q5')
        self.add_request_param(request, 'Q06', 'q6')
        self.add_request_param(request, 'Q07', 'q7')
        self.add_request_param(request, 'Q08', 'q8')
        self.add_request_param(request, 'Q09', 'q9')
        self.add_request_param(request, 'Q10', 'q10')
        self.add_request_param(request, 'Q11', 'q11')
        self.add_request_param(request, 'Q12', 'q12')
        self.add_request_param(request, 'Q13', 'q13')
        self.add_request_param(request, 'Q14', 'q14')
        self.add_request_param(request, 'Q15', 'q15')
        self.add_request_param(request, 'Q16', 'q16')
        self.add_request_param(request, 'Q17', 'q17')
        self.add_request_param(request, 'Q18', 'q18')
        self.add_request_param(request, 'Q19', 'q19')

        self.add_param('CreatedDate', DateUtils.datetime_to_long_string(datetime.datetime.now(), same_timezone=True))
        self.add_param('BookingRequestIP', RequestUtils.get_request_remote_addr(request))

        if len(self._error_list) == 0:
            self.insert_to_db()
            return qid
        else:
            return ''

    def insert_to_db(self):
        cols = ', '.join(self._col_list)
        vals = "','".join(self._val_list)
        vals = "'{0}'".format(vals)
        sql = f"insert into somos_CovidRegQuestionnaire({cols}) values ({vals})"
        print(sql)
        sql = sql.replace("'NULL'", 'NULL')
        ec_db = sql_ec_db()
        ec_db.excute_update(sql)
        ec_db.close_conn()

    def add_request_param(self, request, col, key, data_type='String'):
        self._col_list.append(col)
        val = RequestUtils.get_string(request, key)
        if data_type == 'Date':
            if val == '' or val is None:
                val = 'NULL'
            else:
                val = val.replace('.', '/')
                if len(val) == 8: # 06252020
                    val = val[0:2] + '/' + val[2:4] + '/' + val[4:]
                if val.find('/') > 0:
                    try:
                        dt = datetime.datetime.strptime(val, "%m/%d/%Y")
                        val = DateUtils.datetime_to_short_string(dt)
                    except ValueError:
                        self._error_list.append(f'{col}: {val}, is a not valid date (MM/DD/YYYY)!')
                else:
                    self._error_list.append(f'{col}: {val}, is a not valid date (MM/DD/YYYY)!')
        self._val_list.append(val)

    def add_param(self, col, val):
        self._col_list.append(col)
        self._val_list.append(val)

    def select_booking_info(self, qid):
        #  format(BookingPeriod, 'yyyy-MM-dd HH:mm:ss')
        sql = "select QID, BookingLocationID, BookingDoctorID, BookingPeriod, SiteMode, BookingLockDate, TestingFor from somos_CovidRegQuestionnaire where qid='{0}'".format(qid)
        ec_db = sql_ec_db(as_dict=True)
        res = ec_db.excute_select(sql)
        ec_db.close_conn()
        return res[0] if len(res)>0 else dict()

    def update_booking_info(self, qid, emp_id, ol_id, book_time):
        ec_db = sql_ec_db()
        sql = f"""if not exists (select 1 from somos_CovidRegQuestionnaire where QID ='{qid}' )
                    insert into somos_CovidRegQuestionnaire(QID, CreateByEmployee, CreatedDate, UpdatedDate) values('{qid}', '{emp_id}', GETDATE(), GETDATE())
                """
        ec_db.excute_update(sql)
        sql = f"""update somos_CovidRegQuestionnaire set BookingLocationID={ol_id},BookingDoctorID={emp_id},BookingPeriod='{book_time}', BookingLockDate=getdate() 
                   where qid='{qid}'"""
        print(sql)

        ec_db.excute_update(sql)
        ec_db.close_conn()

    def update_visitor_id(self, qid, vid):
        sql = "update somos_CovidRegQuestionnaire set VisitorID={1} where QID='{0}'".format(qid, vid)
        ec_db = sql_ec_db()
        ec_db.excute_update(sql)
        ec_db.close_conn()

    def update_patient_id(self, qid, pid):
        sql = "update somos_CovidRegQuestionnaire set PatientID={1} where QID='{0}'".format(qid, pid)
        ec_db = sql_ec_db()
        ec_db.excute_update(sql)
        ec_db.close_conn()

    def update_appointment_id(self, qid, app_id, i_understand_testingfor=None):
        sql = "update somos_CovidRegQuestionnaire set AppointmentID={0} ".format(app_id)
        if i_understand_testingfor == 'C':
            sql += ", TestingFor='COVID'"
        elif i_understand_testingfor == 'I':
            sql += ", TestingFor='Immunity'"
        sql += " where QID='{0}'".format(qid)
        ec_db = sql_ec_db()
        ec_db.excute_update(sql)
        ec_db.close_conn()

    def is_vaccination_children_under_5(self, qid):
        sql = "select Q19 from somos_CovidRegQuestionnaire where qid='{0}'".format(qid)
        ec_db = sql_ec_db()
        obj = ec_db.query_first_row_to_dict(sql)
        ec_db.close_conn()
        if obj is not None:
            return obj.get('Q19') == 'ChildrenUnder5'
        return False
