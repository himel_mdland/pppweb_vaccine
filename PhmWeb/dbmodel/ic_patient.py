import datetime, uuid
from PhmWeb.common import RequestUtils, DateUtils
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.utils.logger import Logger
from PhmWeb.dbmodel.ic_patient_ext import IcPatientExt


class IcPatient:
    def __init__(self):
        self._col_list = list()
        self._val_list = list()
        self._param_update = ''
        self._ec_db = sql_ec_db(as_dict=True)

    def register_patient(self, clinic_id, request, visitor_id):
        self._col_list.clear()
        self._val_list.clear()

        self.add_request_param(request, 'PatientFirstName', 'PatientFirstName')
        self.add_request_param(request, 'PatientMidName', 'PatientMidName')
        self.add_request_param(request, 'PatientLastName', 'PatientLastName')
        self.add_request_param(request, 'PatientDOB', 'PatientDOB', 'Date')
        self.add_request_param(request, 'PatientGender', 'PatientGender')
        self.add_request_param(request, 'GenderIdentity', 'GenderIdentity')
        self.add_request_param(request, 'PatientAddrStreet', 'PatientAddrStreet')
        self.add_request_param(request, 'PatientAddrApt', 'PatientAddrApt')
        self.add_request_param(request, 'PatientAddrCity', 'PatientAddrCity')
        self.add_request_param(request, 'PatientAddrState', 'PatientAddrState')
        self.add_request_param(request, 'PatientAddrZip', 'PatientAddrZip')
        self.add_request_param(request, 'PatientHomePhone', 'PatientHomePhone')
        self.add_request_param(request, 'PatientMobilePhone', 'PatientMobilePhone')
        self.add_request_param(request, 'PatientEmail', 'PatientEmail')
        self.add_request_param(request, '[Language]', 'PatientLanguage')
        self.add_request_param(request, 'InsuredFlag', 'HealthInsurance')
        self.add_request_param(request, 'InsuredID', 'HealthInsuranceID')
        self.add_request_param(request, 'InsuredName', 'HealthInsuranceName')
        self.add_request_param(request, 'PatientMemo', 'PatientMemo')

        self.add_request_param(request, 'PCPFlag', 'HasPCP')
        self.add_request_param(request, 'PCPName', 'PCPName')
        self.add_request_param(request, 'PCPNumber', 'PCPNumber')
        self.add_request_param(request, 'PatientSSNumber', 'PatientSSNumber')

        # working info
        self.add_request_param(request, 'PatientWorkPhone', 'PatientWorkPhone')
        self.add_request_param(request, 'PatientWorkPhoneExt', 'PatientWorkPhoneExt')
        self.add_request_param(request, 'EmployerName', 'EmployerName')
        self.add_request_param(request, 'EmploymentCode', 'EmploymentCode')
        self.add_request_param(request, 'Occupation', 'Occupation')
        self.add_request_param(request, 'EmployerAddr', 'EmployerAddr')
        self.add_request_param(request, 'EIN', 'EIN')
        self.add_param('[Notification]', '5')
        self.add_param('CreateByVisitorID', str(visitor_id))
        self.add_param('ClinicID', str(clinic_id))
        self.add_request_param(request, 'PatientAddrCounty', 'PatientAddrCounty')
        self.add_request_param(request, 'NotificationText', 'PatientTextNotif')
        self.add_request_param(request, 'NotificationEmail', 'PatientEmailNotif')
        self.add_request_param(request, 'GuardianConsent', 'GuardianConsent')
        self.add_request_param(request, 'GuardianName', 'GuardianName')
        cols = ', '.join(self._col_list)
        vals = "','".join(self._val_list)
        vals = "'{0}'".format(vals)
        sql = f"insert into Patient({cols}) values ({vals});SELECT scope_identity() as scope_identity"
        Logger(logger_name='ic_patient').getlog().info(sql)

        sql = sql.replace("'NULL'", 'NULL')

        ret = self._ec_db.excute_update(sql, fetchall=True)
        self._ec_db.close_conn()
        pid = 0
        if len(ret) > 0:
            pid = ret[0]['scope_identity']

            pat_ext = IcPatientExt()
            pat_ext.save_patient_race(patientID=pid, clinicID=clinic_id, race_codes=RequestUtils.get_string(request, 'PatientRaceCode'))
            pat_ext.save_patient_ethnicity(patientID=pid, clinicID=clinic_id, ethnicity_codes=RequestUtils.get_string(request, 'PatientEthnicityCode'))

        return pid


    def update_patient(self, request, patient_id, clinic_id):
        self._param_update = ''
        self.build_param_update(request, 'PatientFirstName', 'PatientFirstName')
        self.build_param_update(request, 'PatientMidName', 'PatientMidName')
        self.build_param_update(request, 'PatientLastName', 'PatientLastName')
        self.build_param_update(request, 'PatientDOB', 'PatientDOB', 'Date')
        self.build_param_update(request, 'PatientGender', 'PatientGender')
        self.build_param_update(request, 'GenderIdentity', 'GenderIdentity')
        self.build_param_update(request, 'PatientAddrStreet', 'PatientAddrStreet')
        self.build_param_update(request, 'PatientAddrApt', 'PatientAddrApt')
        self.build_param_update(request, 'PatientAddrCity', 'PatientAddrCity')
        self.build_param_update(request, 'PatientAddrState', 'PatientAddrState')
        self.build_param_update(request, 'PatientAddrZip', 'PatientAddrZip')
        self.build_param_update(request, 'PatientHomePhone', 'PatientHomePhone')
        self.build_param_update(request, 'PatientMobilePhone', 'PatientMobilePhone')
        self.build_param_update(request, 'PatientEmail', 'PatientEmail')
        self.build_param_update(request, '[Language]', 'PatientLanguage')
        self.build_param_update(request, 'InsuredFlag', 'HealthInsurance')
        self.build_param_update(request, 'InsuredID', 'HealthInsuranceID')
        self.build_param_update(request, 'InsuredName', 'HealthInsuranceName')
        self.build_param_update(request, 'PatientMemo', 'PatientMemo')
        self.build_param_update(request, 'PCPFlag', 'HasPCP')
        self.build_param_update(request, 'PCPName', 'PCPName')
        self.build_param_update(request, 'PCPNumber', 'PCPPhone')
        self.build_param_update(request, 'PatientSSNumber', 'PatientSSNumber')
        self.build_param_update(request, 'EIN', 'EIN')
        # working info
        self.build_param_update(request, 'PatientWorkPhone', 'PatientWorkPhone')
        self.build_param_update(request, 'PatientWorkPhoneExt', 'PatientWorkPhoneExt')
        self.build_param_update(request, 'EmployerName', 'EmployerName')
        self.build_param_update(request, 'EmploymentCode', 'EmploymentCode')
        self.build_param_update(request, 'Occupation', 'Occupation')
        self.build_param_update(request, 'EmployerAddr', 'EmployerAddr')
        self.build_param_update(request, 'PatientAddrCounty', 'PatientAddrCounty')
        self.build_param_update(request, 'NotificationText', 'PatientTextNotif')
        self.build_param_update(request, 'NotificationEmail', 'PatientEmailNotif')
        self.build_param_update(request, 'GuardianConsent', 'GuardianConsent')
        self.build_param_update(request, 'GuardianName', 'GuardianName')
        param_update = self._param_update
        sql = f"update Patient set {param_update} where PatientID={patient_id}"
        sql = sql.replace("'NULL'", 'NULL')
        Logger(logger_name='ic_patient').getlog().info(sql)
        self._ec_db.excute_update(sql)
        self._ec_db.close_conn()

        pat_ext = IcPatientExt()
        pat_ext.save_patient_race(patientID=patient_id, clinicID=clinic_id, race_codes=RequestUtils.get_string(request, 'PatientRaceCode'))
        pat_ext.save_patient_ethnicity(patientID=patient_id, clinicID=clinic_id, ethnicity_codes=RequestUtils.get_string(request, 'PatientEthnicityCode'))


    def add_request_param(self, request, col, key, data_type='String'):
        self._col_list.append(col)
        val = RequestUtils.get_safe_sql_string(request, key)

        if data_type == 'Date':
            if val == '' or val is None:
                val = 'NULL'
            elif val.find('.') > 0:
                dt = datetime.datetime.strptime(val, "%m/%d/%Y")
                val = DateUtils.datetime_to_short_string(dt)
            elif len(val) == 8:
                dt = DateUtils.string_to_datetime(val)
                val = DateUtils.datetime_to_short_string(dt)
        if col == 'PatientAddrApt':
            if val and len(val) > 15:
                val = val[0:15]

        self._val_list.append(val.replace("'","''"))

    def build_param_update(self, request, col, key, data_type='String'):
        val = RequestUtils.get_safe_sql_string(request, key)
        if data_type == 'Date':
            if val == '' or val is None:
                val = 'NULL'
            elif val.find('.') > 0:
                dt = datetime.datetime.strptime(val, "%m/%d/%Y")
                val = DateUtils.datetime_to_short_string(dt)
            elif len(val) == 8:
                dt = DateUtils.string_to_datetime(val)
                val = DateUtils.datetime_to_short_string(dt)
        if col == 'PatientAddrApt':
            if val and len(val) > 15:
                val = val[0:15]
        if self._param_update != '':
            self._param_update = self._param_update + ','
        self._param_update = self._param_update + col + '=' + "'" + val + "'"


    def add_param(self, col, val):
        self._col_list.append(col)
        self._val_list.append(val)
