import threading, datetime
from bson.objectid import ObjectId
from PhmWeb.common import DictUtils, DateUtils
from PhmWeb.utils.logger import logger
from PhmWeb.utils.dbconfig import get_PHM_db, get_DW_db


class DbPatCareAction:
    _instance_lock = threading.Lock()

    def __init__(self):
        self._hedis_id = ObjectId('5adfe9b79d10da4bfc3e3582')
        self._dw = None

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPatCareAction, "_instance"):
            with DbPatCareAction._instance_lock:  # keep thread safe , add lock
                if not hasattr(DbPatCareAction, "_instance"):
                    DbPatCareAction._instance = DbPatCareAction(*args, **kwargs)
        return DbPatCareAction._instance


    def save_measure_close_gap(self, mso_pid, measures, status, start_date, end_date, measure_comment):
        self._dw = get_DW_db()

        measure_dict = dict()
        if start_date:
            measure_dict['StartDate'] = DateUtils.string_to_datetime(start_date)
        if end_date:
            measure_dict['EndDate'] = DateUtils.string_to_datetime(end_date)
        if measure_comment:
            measure_dict['Comment'] = measure_comment
        measure_dict['MeasureStatus'] = int(status)
        mso_pat = self._dw['ACO_Summary'].find_one({'_id': ObjectId(mso_pid)}, {'PatientUID': 1, 'Measures': 1, 'ClinicID': 1})
        try:
            self.save_care_action_on_measure(mso_pat, employee_id=-1, action='Close',command=status, op_measure_lis=measures,param_dict={'CloseGap':measure_dict}, channel='PhmWeb')
            return 1
        except Exception as e:
            logger.info('**************phm_pat_CareAction Close Gap*****************')
            logger.info('msopid:' + mso_pid + ' measures:' + str(measures) + ' status:' + str(status))
            logger.info('error:' + str(e))
            return 0

    '''
    since only one measure here , we need to transform the result
    '''
    def save_action_from_qm(self, clinic_id, patient_id, employee_id, action, command, measure):
        self._dw = get_DW_db()
        patient_uid = str(patient_id) + '-' + str(clinic_id)
        mso_pat = self._dw['ACO_Summary'].find_one({'PatientUID': patient_uid},{'PatientUID' :1, 'Measures':1, 'ClinicID':1 })
        op_result = self.save_care_action_on_measure(mso_pat, employee_id, action, command, [measure], param_dict=None, channel='QM')
        #
        if op_result is None:
            return None
        op_result['mr_result'] = op_result['mr_result'][measure] if measure in op_result['mr_result'] else None
        return op_result

    '''
    this is private method
    {'mso_pid': 'mso_sumary._id', mr_result: {'ABA': {}, 'BCS': {}}}
    '''
    def save_care_action_on_measure(self, mso_pat, employee_id, action, command, op_measure_lis, param_dict=None, channel='QM'):
        zw = get_PHM_db()
        if not mso_pat:
            return None
        operate_date = datetime.datetime.now()
        patient_uid = mso_pat['PatientUID']

        mso_pid = mso_pat['_id']
        action_dict = dict()
        action_dict['mso_pid'] = mso_pid
        action_dict['PatientUID'] = patient_uid
        action_dict['ClinicID'] = mso_pat['ClinicID']
        action_dict['PcpID'] = employee_id
        action_dict['CareType'] = 'QM'
        action_dict['Channel'] = channel
        action_dict['Action'] = action
        action_dict['Command'] = command
        action_dict['Measure'] = ','.join(op_measure_lis)
        action_dict['CreatedDate'] = operate_date
        if param_dict is not None:
            for param in param_dict:
                action_dict[param] = param_dict[param]
        zw['phm_pat_CareAction'].insert(action_dict)

        res = dict()
        res['mso_pid'] = mso_pid
        res['mr_result'] = dict()

        for op_measure in op_measure_lis:
            if op_measure is None or len(op_measure) < 3:
                continue
            measures = mso_pat['Measures']
            for mr in measures:
                if mr['Measure'] == op_measure:
                    self.change_measure_stauts(mr, action, command)
                    mr['OperateDate'] = operate_date
                    res['mr_result'][op_measure] = mr
                    break
        self._dw['ACO_Summary'].update({'_id': mso_pid}, {'$set': {'Measures': measures}})
        return res

    '''
    1. only Close, OptOut, Exclude will directly change the measure's status
    when a document is uploaded, no change will made to the measure, but backend thread will recalculate the patient's measure
    
    '''
    def change_measure_stauts(self, mr, action, command):
        mr['ActionName'] = action
        if command is not None and len(command) > 0:
            mr['ActionCommand'] = command
        else:
            if 'ActionCommand' in mr:
                del mr['ActionCommand']
        # begin if: business for different measure
        if action in ['Close']:
            mr['Numerator'] = 'Y'
        elif action in ['OptOut', 'Exclude']:
            if command == 'Reset':
                mr['Exclusion'] = 'N'
                mr['Denominator'] = 'Y'
            else:
                mr['Exclusion'] = 'Y'
                mr['Denominator'] = 'N'
        #
        # elif action in ['UploadReport']:

        # end if: business for different measure

        mr['ActionBy'] = 'QM: {0} {1}'.format(action, ('({0})'.format(command) if command is not None and len(command) > 0 else ''))




